#ifndef OARA__ACTOR_CLIENT_HPP_
#define OARA__ACTOR_CLIENT_HPP_

#include <chrono>
#include <rclcpp/rclcpp.hpp>
#include <oara_interfaces/msg/goal_request.hpp>
#include <oara_interfaces/msg/goal_state.hpp>
#include "oara/thread_safe_queue.hpp"

namespace oara
{

class AbstractActorClient
{
public:
  AbstractActorClient(rclcpp::Node::SharedPtr node, const std::string& name, int timeout_sec);

  virtual ~AbstractActorClient();

  virtual bool select(const std::string& goal_id) = 0;
  virtual bool expand(const std::string& goal_id) = 0;
  virtual bool commit(const std::string& goal_id, const std::string& plan_id) = 0;
  virtual bool dispatch(const std::string& goal_id) = 0;
  virtual bool drop(const std::string& goal_id) = 0;
  
  oara_interfaces::msg::GoalState result(const std::string& goal_id);

  bool traverse_lifecycle(const std::string& task_id);

  bool process_event(const std::string& goal_id);
  bool wait_connection();
  
  oara_interfaces::msg::GoalState get_state(std::string id);

protected:
  rclcpp::Node::SharedPtr node;
  std::string name;
  std::map<std::string, oara_interfaces::msg::GoalState> goal_states;
  std::map<std::string, oara::queue<oara_interfaces::msg::GoalState> > goal_events;
  rclcpp::CallbackGroup::SharedPtr mutex_cb_group;
  rclcpp::QoS qos;
  bool alive;

private:
  std::chrono::seconds timeout;
  rclcpp::Subscription<oara_interfaces::msg::GoalState>::SharedPtr feedback_subscriber;
  rclcpp::SubscriptionOptions feedback_options_;
  void feedback_callback(const oara_interfaces::msg::GoalState::SharedPtr msg);
  void feedback_liveliness(rclcpp::QOSLivelinessChangedInfo& event);

};


template<typename T, typename U>
class ActorClient : public AbstractActorClient
{
public:
  ActorClient(rclcpp::Node::SharedPtr node, const std::string& name, int timeout_sec);
  virtual ~ActorClient();

  bool formulate(const std::string& goal_id, const U& goal);
  virtual bool select(const std::string& goal_id);
  virtual bool expand(const std::string& goal_id);
  virtual bool commit(const std::string& goal_id, const std::string& plan_id);
  virtual bool dispatch(const std::string& goal_id);
  virtual bool drop(const std::string& goal_id);
  virtual bool traverse_lifecycle(const std::string& task_id, const U& goal);

private:
  typename rclcpp::Publisher<T>::SharedPtr request_publisher;
};

}  // namespace oara

#endif  // OARA__ACTOR_CLIENT_HPP_
