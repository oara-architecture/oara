#ifndef OARA__DATA_CLIENT_HPP_
#define OARA__DATA_CLIENT_HPP_

#include <chrono>
#include <mutex>
#include <condition_variable>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/header.hpp>
#include <oara_interfaces/msg/data_trigger.hpp>

namespace oara
{

class AbstractDataClient
{
public:
  AbstractDataClient(rclcpp::Node::SharedPtr node, const std::string& observer, int timeout_sec);

  virtual ~AbstractDataClient();

  bool wait_connection();

protected:
  rclcpp::Node::SharedPtr node;
  std::string observer;
  std::map<std::string, std_msgs::msg::Header> headers;
  rclcpp::CallbackGroup::SharedPtr cb_group;
  rclcpp::QoS qos;
  bool alive;

  rclcpp::Publisher<oara_interfaces::msg::DataTrigger>::SharedPtr trigger_publisher;
  rclcpp::SubscriptionOptions subscriber_options_;
  
  std::map<std::string, rclcpp::Subscription<std_msgs::msg::Header>::SharedPtr> header_subscribers;
  void header_callback(std::string data, const std_msgs::msg::Header::SharedPtr msg);

  std::map<std::string, std::mutex> mutex;
  std::map<std::string, std::condition_variable> value_conditions;
  std::map<std::string, std::condition_variable> header_conditions;
  std::map<std::string, bool> header_received;
  std::map<std::string, bool> value_received;

private:
  std::chrono::seconds timeout;
  void feedback_liveliness(rclcpp::QOSLivelinessChangedInfo& event);
};


template<typename T>
class DataClient : public AbstractDataClient
{
public:
  DataClient(rclcpp::Node::SharedPtr node, const std::string& observer, int timeout_sec);
  virtual ~DataClient();

  bool add_data(const std::string& data, const std::string& remote);
  std::pair<std_msgs::msg::Header, T> get_data(const std::string& data);

private:
  std::map<std::string, typename rclcpp::Subscription<T>::SharedPtr> value_subscribers;
  std::map<std::string, T> values;

  void value_callback(std::string data, const typename T::SharedPtr msg);
};

}  // namespace oara

#include <oara/impl/data_client.hpp>

#endif  // OARA__DATA_CLIENT_HPP_
