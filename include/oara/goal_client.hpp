#ifndef OARA__GOAL_CLIENT_HPP_
#define OARA__GOAL_CLIENT_HPP_

#include <chrono>
#include <rclcpp/rclcpp.hpp>
#include <oara_interfaces/msg/goal_state.hpp>
#include "oara/thread_safe_queue.hpp"

namespace oara
{

class GoalClient
{
public:
  using Timeline = std::map<double, int>;
  
  GoalClient(rclcpp::Node::SharedPtr node, const std::string& name, int timeout_sec = 1);

  virtual ~GoalClient();

  bool process_event(const std::string& goal_id);
  bool wait_connection();
  
  oara_interfaces::msg::GoalState get_state(std::string id) const;
  std::vector<std::string> get_goal_ids() const;

  inline Timeline get_timeline(const std::string& goal_id) const { return timelines.at(goal_id); };
  void clear_timelines();

protected:
  rclcpp::Node::SharedPtr node;
  std::string name;
  std::map<std::string, oara_interfaces::msg::GoalState> goal_states;
  std::map<std::string, oara_interfaces::msg::GoalState> goal_provessed_event;
  std::map<std::string, oara::queue<oara_interfaces::msg::GoalState> > goal_events;
  rclcpp::CallbackGroup::SharedPtr mutex_cb_group;
  rclcpp::QoS qos;
  bool alive;

private:
  std::chrono::seconds timeout;
  rclcpp::Subscription<oara_interfaces::msg::GoalState>::SharedPtr feedback_subscriber;
  rclcpp::SubscriptionOptions feedback_options_;
  void feedback_callback(const oara_interfaces::msg::GoalState::SharedPtr msg);
  void feedback_liveliness(rclcpp::QOSLivelinessChangedInfo& event);

  std::map<std::string, Timeline> timelines;

};

}  // namespace oara

#endif  // OARA__ACTOR_CLIENT_HPP_
