#include <string>
#include <iostream>
#include <sstream>

#include <rclcpp/logging.hpp>
#include <oara_interfaces/msg/goal_request.hpp>
#include <oara_interfaces/msg/goal_state.hpp>
#include <std_msgs/msg/string.hpp>

#include "oara/actor_client.hpp"
#include "oara/utils.hpp"

using std::placeholders::_1;

namespace oara
{

template <typename T, typename U>
ActorClient<T, U>::ActorClient(rclcpp::Node::SharedPtr node, 
    const std::string& name, int timeout_sec)
    : AbstractActorClient(node, name, timeout_sec)
{
    std::stringstream sform; sform << name << "/oara/request";
    this->request_publisher = this->node->template create_publisher<T>(sform.str(), qos);
}

template <typename T, typename U>
ActorClient<T, U>::~ActorClient() {
}

template <typename T, typename U>
bool ActorClient<T, U>::formulate(const std::string& goal_id, const U& goal) {
    T request;
    request.request.header.frame_id = this->node->get_name();
    request.request.header.stamp = this->node->get_clock()->now();
    request.request.goal_id = goal_id;
    request.request.step = oara_interfaces::msg::GoalRequest::FORMULATE;
    request.goal = goal;

    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] Calling 'formulate' with request " << request.request.goal_id);
    if (! this->alive) {
        RCLCPP_ERROR_STREAM(node->get_logger(), "[client " << this->name << "] not connected to " << this->name);
        return false;
    }
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] Publishing request");
    this->request_publisher->publish(request);
    auto msg = this->goal_events[goal_id].pop();
    RCLCPP_DEBUG_STREAM(this->node->get_logger(), "[client " << this->name << "] Received response " << msg.goal_id << " state " << state2string(msg.state));
    this->goal_states[msg.goal_id] = msg;
    return (msg.state == oara_interfaces::msg::GoalState::FORMULATED);
}

template <typename T, typename U>
bool ActorClient<T, U>::select(const std::string& goal_id) {
    T request;
    request.request.header.frame_id = this->node->get_name();
    request.request.header.stamp = this->node->get_clock()->now();
    request.request.goal_id = goal_id;
    request.request.step = oara_interfaces::msg::GoalRequest::SELECT;

    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] Calling 'select' with request " << request.request.goal_id);
    if (! this->alive) {
        RCLCPP_ERROR_STREAM(node->get_logger(), "[client " << this->name << "] not connected to " << this->name);
        return false;
    }
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] Publishing request");
    this->request_publisher->publish(request);
    oara_interfaces::msg::GoalState msg =  this->goal_events[goal_id].pop();
    RCLCPP_DEBUG_STREAM(this->node->get_logger(), "[client " << this->name << "] Received response " << msg.goal_id << " state " << state2string(msg.state));
    this->goal_states[msg.goal_id] = msg;
    return (msg.state == oara_interfaces::msg::GoalState::SELECTED);
}

template <typename T, typename U>
bool ActorClient<T, U>::expand(const std::string& goal_id) {
    T request;
    request.request.header.frame_id = this->node->get_name();
    request.request.header.stamp = this->node->get_clock()->now();
    request.request.goal_id = goal_id;
    request.request.step = oara_interfaces::msg::GoalRequest::EXPAND;

    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] Calling 'expand' with request " << request.request.goal_id);
    if (! this->alive) {
        RCLCPP_ERROR_STREAM(node->get_logger(), "[client " << this->name << "] not connected to " << this->name);
        return false;
    }
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] Publishing request");
    this->request_publisher->publish(request);
    oara_interfaces::msg::GoalState msg =  this->goal_events[goal_id].pop();
    RCLCPP_DEBUG_STREAM(this->node->get_logger(), "[client " << this->name << "] Received response " << msg.goal_id << " state " << state2string(msg.state));
    this->goal_states[msg.goal_id] = msg;
    return (msg.state == oara_interfaces::msg::GoalState::EXPANDED);
}

template <typename T, typename U>
bool ActorClient<T, U>::drop(const std::string& goal_id) {
    T request;
    request.request.header.frame_id = this->node->get_name();
    request.request.header.stamp = this->node->get_clock()->now();
    request.request.goal_id = goal_id;
    request.request.step = oara_interfaces::msg::GoalRequest::DROP;

    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] Calling 'drop' with request " << request.request.goal_id);
    if (! this->alive) {
        RCLCPP_ERROR_STREAM(node->get_logger(), "[client " << this->name << "] not connected to " << this->name);
        return false;
    }
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] Publishing request");
    this->request_publisher->publish(request);
    oara_interfaces::msg::GoalState msg =  this->goal_events[goal_id].pop();
    RCLCPP_DEBUG_STREAM(this->node->get_logger(), "[client " << this->name << "] Received response " << msg.goal_id << " state " << state2string(msg.state));
    this->goal_states[msg.goal_id] = msg;
    return (msg.state == oara_interfaces::msg::GoalState::DROPPED);
}

template <typename T, typename U>
bool ActorClient<T, U>::dispatch(const std::string& goal_id) {
    T request;
    request.request.header.frame_id = this->node->get_name();
    request.request.header.stamp = this->node->get_clock()->now();
    request.request.goal_id = goal_id;
    request.request.step = oara_interfaces::msg::GoalRequest::DISPATCH;

    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] Calling 'dispatch' with request " << request.request.goal_id);
    if (! this->alive) {
        RCLCPP_ERROR_STREAM(node->get_logger(), "[client " << this->name << "] not connected to " << this->name);
        return false;
    }
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] Publishing request");
    this->request_publisher->publish(request);
    oara_interfaces::msg::GoalState msg =  this->goal_events[goal_id].pop();
    RCLCPP_DEBUG_STREAM(this->node->get_logger(), "[client " << this->name << "] Received response " << msg.goal_id << " state " << state2string(msg.state));
    this->goal_states[msg.goal_id] = msg;
    return (msg.state == oara_interfaces::msg::GoalState::DISPATCHED);
}

template <typename T, typename U>
bool ActorClient<T, U>::commit(const std::string& goal_id, const std::string& plan_id) {
    T request;
    request.request.header.frame_id = this->node->get_name();
    request.request.header.stamp = this->node->get_clock()->now();
    request.request.goal_id = goal_id;
    request.request.plan_id = plan_id;
    request.request.step = oara_interfaces::msg::GoalRequest::COMMIT;

    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] Calling 'commit' with request " << request.request.goal_id);
    if (! this->alive) {
        RCLCPP_ERROR_STREAM(node->get_logger(), "[client " << this->name << "] not connected to " << this->name);
        return false;
    }
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] Publishing request");
    this->request_publisher->publish(request);
    oara_interfaces::msg::GoalState msg =  this->goal_events[goal_id].pop();
    RCLCPP_DEBUG_STREAM(this->node->get_logger(), "[client " << this->name << "] Received response " << msg.goal_id << " state " << state2string(msg.state));
    this->goal_states[msg.goal_id] = msg;
    return (msg.state == oara_interfaces::msg::GoalState::COMMITTED);
}


template <typename T, typename U>
bool ActorClient<T, U>::traverse_lifecycle(const std::string& task_id, const U& goal) {
    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] formulating task " << task_id);
    auto formuate_result = this->formulate(task_id, goal);
    if (! formuate_result) return false;
    else return AbstractActorClient::traverse_lifecycle(task_id);
}

}  // namespace oara
