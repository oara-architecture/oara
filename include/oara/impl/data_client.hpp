#include <string>
#include <iostream>
#include <sstream>
#include <mutex>
#include <condition_variable>

#include <rclcpp/logging.hpp>

#include "oara/data_client.hpp"
#include "oara/utils.hpp"

namespace oara
{

template <typename T>
DataClient<T>::DataClient(rclcpp::Node::SharedPtr node, 
    const std::string& observer, int timeout_sec)
    : AbstractDataClient(node, observer, timeout_sec)
{
}

template <typename T>
DataClient<T>::~DataClient() {
}

template <typename T>
bool DataClient<T>::add_data(const std::string& data, const std::string& remote)
{
    auto i = value_subscribers.find(data);
    if (i != value_subscribers.end()) {
        RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->observer << "] data " << data << " already registered");
        return false;
    }

    std::stringstream sv; sv << this->observer << "/data/" << remote << "/value";
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] creating subscriber " << sv.str());
    value_subscribers[data] = this->node->template create_subscription<T>(sv.str(),
        qos, 
        [this, data] (typename T::SharedPtr msg) { this->value_callback(data, msg); },
        subscriber_options_);

    std::stringstream sh; sh << this->observer << "/data/" << remote << "/header";
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] creating subscriber " << sh.str());
    header_subscribers[data] = this->node->template create_subscription<std_msgs::msg::Header>(sh.str(),
        qos,
        [this, data] (std_msgs::msg::Header::SharedPtr msg) { this->header_callback(data, msg); });
        //subscriber_options_);

    mutex[data];
    value_conditions[data];
    header_conditions[data];
    return true;
}

template <typename T>
void DataClient<T>::value_callback(std::string data, const typename T::SharedPtr msg) {
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] received data " << data << " value");
    this->values[data] = *msg;
    std::unique_lock<std::mutex> lk(mutex[data]);
    this->value_received[data] = true;
    lk.unlock();
    this->value_conditions[data].notify_one();
}

template <typename T>
std::pair<std_msgs::msg::Header, T> DataClient<T>::get_data(const std::string& data) {
    this->header_received[data] = false;
    this->value_received[data] = false;

    oara_interfaces::msg::DataTrigger t;
    t.data = data;
    t.header.frame_id = this->node->get_name();
    t.header.stamp = this->node->get_clock()->now();
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] publisher trigger for data " << data);
    trigger_publisher->publish(t);

    std::unique_lock<std::mutex> lk(mutex[data]);
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] waiting value for data " << data);
    this->value_conditions[data].wait(lk, [this, data]{ return this->value_received[data]; });
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] waiting header for data " << data);
    this->header_conditions[data].wait(lk, [this, data]{ return this->header_received[data]; });
    lk.unlock();
    return std::make_pair(headers[data], values[data]);
}


}  // namespace oara
