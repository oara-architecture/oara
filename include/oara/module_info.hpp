#ifndef OARA__MODULE_INFO_HPP_
#define OARA__MODULE_INFO_HPP_

#include <chrono>
#include <rclcpp/rclcpp.hpp>
#include <lifecycle_msgs/srv/change_state.hpp>
#include <oara_interfaces/msg/module_info.hpp>

#include "goal_client.hpp"

namespace oara
{

class ModuleInfoClient
{
public:
  ModuleInfoClient(rclcpp::Node::SharedPtr node);

  virtual ~ModuleInfoClient();

  std::string get_state_str(const std::string& name) const;
  bool is_configured(const std::string& name) const;
  bool is_finalized(const std::string& name) const;
  bool is_active(const std::string& name) const;
  bool is_actor(const std::string& name) const;

  std::vector<std::string> get_module_names() const;
  oara_interfaces::msg::ModuleInfo get_info(const std::string& name) const;

  inline bool configure_module(const std::string& name) {
    if (this->is_configured(name)) return true;
    else return this->change_module_state(name, "configure");
  };

  inline bool activate_module(const std::string& name) {
    if (this->is_active(name)) return true;
    else return this->change_module_state(name, "activate");
  };

  inline bool cleanup_module(const std::string& name) {
    if (! this->is_configured(name)) return true;
    else return this->change_module_state(name, "cleanup");
  };

  inline bool deactivate_module(const std::string& name) {
    if (!this->is_active(name)) return true;
    else return this->change_module_state(name, "deactivate");
  };

  inline bool shutdown_module(const std::string& name) {
    return this->change_module_state(name, "shutdown");
  };

  std::vector<oara_interfaces::msg::GoalState> get_goals(const std::string& name) const;
  void clear_goal_timeline(const std::string& name);
  GoalClient::Timeline get_goal_timeline(const std::string& name, const std::string& goal_id) const;

private:
  rclcpp::Node::SharedPtr node;
  rclcpp::Subscription<oara_interfaces::msg::ModuleInfo>::SharedPtr module_info_subscription;
  void module_info_callback(const oara_interfaces::msg::ModuleInfo::SharedPtr msg);
  rclcpp::CallbackGroup::SharedPtr info_cb_group;
  rclcpp::QoS qos;

  std::map<std::string, oara_interfaces::msg::ModuleInfo> module_info;
  std::map<std::string, oara_interfaces::msg::ModuleInfo> actor_info;
  std::map<std::string, oara_interfaces::msg::ModuleInfo> observer_info;

  std::map<std::string, rclcpp::Client<lifecycle_msgs::srv::ChangeState>::SharedPtr> change_state_clients;
  bool change_module_state(const std::string& name, const std::string& transition);

  std::map<std::string, std::shared_ptr<oara::GoalClient>> goal_clients;

};

}  // namespace oara

#endif  // OARA__MODULE_INFO_HPP_
