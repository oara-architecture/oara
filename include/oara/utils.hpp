#pragma once

#include <string>
#include <oara_interfaces/msg/goal_state.hpp>

namespace oara {

std::string new_id();

std::string state2string(int state);
std::string state2string(const oara_interfaces::msg::GoalState& msg);

}