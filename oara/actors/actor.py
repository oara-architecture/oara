from abc import abstractmethod
from functools import partial
from typing import Generic, List, Optional, Tuple, TypeVar, Union, final, Dict, Any, MutableSet
from collections import defaultdict
from heapq import heapify
import traceback

import oara
from oara.internal.plan import Plan

from rclpy_lifecycle import State
from rclpy.parameter import Parameter
from rclpy.callback_groups import ReentrantCallbackGroup, MutuallyExclusiveCallbackGroup

from std_msgs.msg import Header
from oara_interfaces.msg import (
    ModuleInfo, DataInfo, ChildInfo, GoalState, GoalRequest, ResolveTo,
    Expansion, EventInfo, Event)

from ..internal import GoalRequestMsg, Module, MsgType, CallbackReturn, UID
from ..internal.qos import HARD_STATE_QOS, EVENT_QOS

from ..internal.event import EventReaction
from ..internal.report import DispatchReport
from ..internal.actor_client import ActorClient
from ..internal.data_client import DataClient

from ..internal import SrvType


T = TypeVar('T')


class Actor(Module, Generic[T]):
    """ A ROS2 OARA Actor.

    It is implemented as a ROS2 node that provides topics to handle goal requests,
    and delegates subgoals through clients.
    """

    def __init__(self, name: str, request_msg: T,
                 select_unique: bool = False,
                 commit_unique: bool = False):
        """
        :param name: the actor name.
        :param request_msg: the ROS2 message corresponding to request on goals.
        :param select_unique: if true, only one goal at a time can pass the selected step.
        :param commit_unique: if true, only one goal at a time can pass the commited step.
        """
        super().__init__(name)
        self._model.type = ModuleInfo.ACTOR
        self._model.goal_type = request_msg.__name__

        self.__select_unique = select_unique
        self.__commit_unique = commit_unique
        self.__request_msg = request_msg

        self.__data_types : Dict[str, MsgType] = dict()
        self.__data_observers : Dict[str, str] = dict()
        self.__data_clients : Dict[str, DataClient] = dict()
        self.__data_cbgroup = ReentrantCallbackGroup()

        self.__event_strategies = dict()
        self.__event_cbgroup = MutuallyExclusiveCallbackGroup()

        self.add_parameter('oara.auto_resolve', Parameter.Type.BOOL, default_value=True,
            description="When resolving a goal, tries automatically to dispatch it again")
        self.add_parameter('oara.replan_choice', Parameter.Type.STRING, default_value='random',
            description='When auto replanning, what is the strategy to choice among the new plans? one of "random", "min", "max"')
        self.add_parameter('oara.connection_timeout', Parameter.Type.DOUBLE, default_value=5.0,
            description='Timeout when establishing connections to other modules')

        self.__goals : Dict[UID, T] = dict()
        self.__goal_states : Dict[UID, int] = dict()
        self.__children_msg : Dict[str, MsgType] = dict()
        self.__children_client : Dict[str, ActorClient] = dict()
        self.__response_msgs : 'List[GoalState]' = []
        self.__pending_ext_requests : 'List[GoalRequestMsg[T]]' = []  # Pending external requests
        self.__pending_int_requests : 'List[GoalRequestMsg[T]]' = []  # Pending internal requests
        self.__pending_eval_requests : 'List[Tuple[UID, DispatchReport]]' = []  # Pending requests to call the evaluate function
        self.__expansions : Dict[UID, Dict[UID, Tuple[Plan, int]]] = defaultdict(dict)
        self.__dispatched_expansions : Dict[UID, MutableSet[UID]] = defaultdict(set)  # Plans that have already been tried
        self.__committed_plan : Dict[UID, Tuple[UID, Plan]] = dict()

    def on_configure(self, state: State) -> CallbackReturn:
        cb_success = super().on_configure(state)
        if cb_success == CallbackReturn.SUCCESS:
            self.model.auto_resolve = self.get_parameter('oara.auto_resolve').value
            self.get_logger().debug(f"-- auto resolve: {self.model.auto_resolve}")
            self.model.connection_timeout = self.get_parameter('oara.connection_timeout').value
            self.get_logger().debug(f"-- connection timeout: {self.model.connection_timeout}")
            self.model.replan_choice = self.get_parameter('oara.replan_choice').value
            self.get_logger().debug(f"-- replan choice: {self.model.replan_choice}")

            self.__request_subscriber = self.create_subscription(self.__request_msg, '~/oara/request',
                self.__request_callback,
                qos_profile=HARD_STATE_QOS)
            self.__response_publisher = self.create_publisher(GoalState, '~/oara/response',
                qos_profile=HARD_STATE_QOS)

        return cb_success

    ###------------------------ DATA ---###
    @final
    def add_data(self, name: str, msg: MsgType) -> bool:
        """ Add a new **Data** to the actor.

        New data can be added only before activating the actor.

        :param name: data name.
        :param msg: ROS2 message type of the data.
        :return: True if the data has been added.
        """
        if name in self.__data_types:
            self.get_logger().warn(f"data {name} already added")
            return False

        if self.active:
            self.get_logger().warn(f"cannot add new data when the module is active")
            return False

        self.__data_types[name] = msg
        self.get_logger().debug(f"added data {name}")
        self.model.data.append(DataInfo(name=name, type=msg.__name__))

        self.add_parameter(f'{name}.observer', type=Parameter.Type.STRING,
            description=f'name of the observer node providing data {name}')
        self.add_parameter(f'{name}.data', type=Parameter.Type.STRING, default_value=name,
            description=f'name of the {name} data on the observer node')
        return True

    @final
    def get_data(self, name: str) -> Tuple[Header, MsgType]:
        """ Get the value of a **Data**.

        :param name: data name.
        """
        if not self.active:
            self.get_logger().error("Cannot get data when not active")
            return None

        if name not in self.__data_observers:
            self.get_logger().error(f"Data {name} unknown")
            return None

        obs = self.__data_observers[name]
        client = self.__data_clients[obs]
        if not client.wait_connection(timeout=0.0):
            self.get_logger().error(f"Data {name} not connected to observer {obs}!")
            return None

        return client.get_data(name)

    @final
    def call_data(self, name: str, fun: str, srv: SrvType, request: MsgType) -> Any:
        """ Call a function attached to a **Data**.

        :param name: data name.
        :param fun: function name.
        :param srv: ROS2 type of the function service.
        :param request: service request message.
        """
        # FIX NEEDED
        #if not self.active:
        #    self.get_logger().error("Cannot call data when not active")
        #    return None

        if not name in self.__data_types:
            self.get_logger().error(f"Data {name} not registered!")
            return

        if self.is_parameter_set(f'{name}.observer') and self.is_parameter_set(f'{name}.data'):
            provider = self.get_parameter(f'{name}.observer').value
            data = self.get_parameter(f'{name}.data').value
        else:
            self.get_logger().error("Connection not established with observer")
            return None

        client = self.create_client(srv, f"{provider}/{data}/{fun}", callback_group=self.__data_cbgroup)
        connected = client.wait_for_service(timeout_sec=1.0)
        if connected:
            self.get_logger().debug(f"call {data}/{fun} provider")
            response = client.call(request)
            self.get_logger().debug(f"received call {name}/{fun} response {response}")
            return response
        else:
            raise TimeoutError()

    ###------------------------ EVENTS ---###
    @final
    def add_event(self, name: str,
                  strategy: EventReaction = EventReaction.PROCESS) -> bool:
        """ Add a new **Event** to the actor.

        New events can be added only before activating the actor.

        :param name: event name.
        :param strategy: strategy to apply when the event is received. default is PROCESS.
        :return: True if the event has been added.
        """
        if self.active:
            self.get_logger().error("Cannot add new event while active!")
            return False

        self.__event_strategies[name] = strategy
        self.add_parameter(f'{name}.observer', type=Parameter.Type.STRING,
                description=f'name of the observer/data emitting event {name}')
        self.add_parameter(f'{name}.event', type=Parameter.Type.STRING, default_value=name,
                description=f'name of the {name} event on the observer node')
        self.model.events.append(EventInfo(name=name, strategy=strategy.name))
        return True

    ###------------------------ CLIENTS ---###
    @final
    def add_child_actor(self, name: str, request_msg: MsgType) -> bool:
        """ Add a new client for this actor.

        A client is a child actor to which goals can be formulated.

        :param name: child name.
        :param request_msg: ROS2 type of the topic accepting goal requests on the child side.
        """
        if name in self.__children_msg:
            self.get_logger().warn(f"child actor {name} already added")
            return False

        if self.active:
            self.get_logger().error("Cannot add new child actor while active!")
            return False

        self.add_parameter(f'{name}.actor', type=Parameter.Type.STRING,
                description=f'name of actor {name} node')
        self.__children_msg[name] = request_msg
        self._model.children.append(ChildInfo(name=name, type=request_msg.__name__))
        return True

    def get_actor_client(self, name: str) -> ActorClient:
        """ Get the client managing a child.
        
        :param name: the child name.
        :return: the child actor client.
        """
        return self.__children_client[name]

    def __update_model_child(self, name: str, node: str) -> bool:
        for child in self.model.children:
            if child.name == name:
                child.node = node
                return True
        return False

    def __update_model_data(self, name: str, node: str, remote: str) -> bool:
        for data in self.model.data:
            if data.name == name:
                data.node = node
                data.data = remote
                return True
        return False

    def __update_model_event(self, name: str, node: str, remote: str) -> bool:
        for e in self.model.events:
            if e.name == name:
                e.data = f"{node}/{remote}"
                return True
        return False

    def on_activate(self, state: State) -> CallbackReturn:
        cb_success = super().on_activate(state)
        if cb_success == CallbackReturn.SUCCESS:
            # Tentative to resolve remote nodes namespaces
            # node_names_and_namespaces : Dict[str, str] = dict(self.get_node_names_and_namespaces())

            for name, msg in self.__children_msg.items():
                self.get_logger().debug(f"connecting clients for child actor {name}")
                if not self.is_parameter_set(f'{name}.actor'):
                    self.get_logger().warn(f"actor {name} not set: ignoring")
                    continue
                actor = self.get_parameter(f'{name}.actor').value
                self.get_logger().debug(f"Connect actor child {name} to actor node {actor}")
                client = ActorClient(self, actor, msg)
                if not client.wait_connection(self.model.connection_timeout):
                    self.get_logger().error(f"impossible to connect to {actor}")
                    return CallbackReturn.FAILURE
                self.__children_client[name] = client
                self.__update_model_child(name, actor)

            for name, msg_type in self.__data_types.items():
                if not self.is_parameter_set(f'{name}.observer'):
                    self.get_logger().warn(f"observer for data {name} not set: ignoring")
                    continue
                observer : str = self.get_parameter(f'{name}.observer').value
                remote_data: str = self.get_parameter(f'{name}.data').value
                self.get_logger().debug(f"data {name} bound to observer {observer} / {remote_data}")
                self.__data_observers[name] = observer
                if observer not in self.__data_clients:
                    self.__data_clients[observer] = DataClient(self, observer)
                self.get_logger().debug(f"created data client to observer {observer}")
                self.__data_clients[observer].add_data(name, msg_type, remote_data)
                self.__update_model_data(name, observer, remote_data)

            for obs, client in self.__data_clients.items():
                self.get_logger().debug(f"connecting client for observer {obs}")
                if not client.wait_connection(self.model.connection_timeout):
                    self.get_logger().error(f"impossible to connect to {observer}")
                    return CallbackReturn.FAILURE
            
            for name in self.__event_strategies:
                if not self.is_parameter_set(f'{name}.observer'):
                    self.get_logger().warn(f"observer for event {name} not set: ignoring")
                    continue
                provider = self.get_parameter(f'{name}.observer').value
                event = self.get_parameter(f'{name}.event').value
                self.create_subscription(Event, f"{provider}/event/{event}",
                                        callback=partial(self.__process_event, name),
                                        qos_profile=EVENT_QOS,
                                        callback_group=self.__event_cbgroup)
                self.__update_model_event(name, observer, event)
                self.get_logger().debug(f"creating subscriber for event {name}")
            
        return cb_success

    def on_deactivate(self, state: State) -> CallbackReturn:
        cb_success = super().on_deactivate(state)
        if cb_success == CallbackReturn.SUCCESS:
            for name, client in self.__children_client.items():
                self.get_logger().debug(f"destroying clients for child actor {name}")
                client.destroy()
            self.__children_client.clear()
        return cb_success

    ###------------------------ REQUESTS ---###
    def __request_callback(self, request: GoalRequestMsg[T]):
        if request.request.header.frame_id == self.get_name():
            # The request is internal when the sender has the same name as this node
            self.__pending_int_requests.append(request)
        else:
            self.__pending_ext_requests.append(request)

    def _request_internal_transition(self, request: GoalRequestMsg[T]):
        self.__pending_int_requests.append(request)

    @final
    def request_evaluate(self, gid: UID, report: DispatchReport):
        self.__pending_eval_requests.append((gid, report))

    @final
    def _loop(self):
        super()._loop()

        # Loop processing order:
        # * external transitions
        # * goal monitoring
        # * goal reports evaluation
        # * internal transitions

        # Process external lifecycle transition requests
        self.get_logger().debug(f"{len(self.__pending_ext_requests)} pending external requests")
        try:
            req = self.__pending_ext_requests.pop()
            self.__process_transition_request(req)
        except IndexError:
            pass
        except:
            self.console.print_exception(show_locals=True)

        self.__flush_responses()

        # Monitor dispatched goals
        dispatched_goals = self.get_dispatched_goals()
        self.get_logger().debug(f"monitoring {len(dispatched_goals)} dispatched goals")
        for gid in dispatched_goals:
            try:
                self.monitor(gid)
            except Exception as ex:
                self.console.print_exception(show_locals=True)

        self.__flush_responses()

        # Call evaluate function if requested
        self.get_logger().debug(f"{len(self.__pending_eval_requests)} pending eval requests")
        try:
            gid, dispatch_report = self.__pending_eval_requests.pop()
            can_eval = self.__to_evaluated(gid, dispatch_report)  # Advance to the EVALUATED status
            self.__flush_responses()  # Disseminate the EVALUATED status
            if can_eval:  # If the EVALUATED status has been reached then
                self.__evaluate(gid, dispatch_report)  # Execute the evaluate function
        except IndexError:
            pass
        except:
            self.console.print_exception(show_locals=True)

        self.__flush_responses()  # Disseminate the status after evaluation

        # Handle internal transition requests
        self.get_logger().debug(f"{len(self.__pending_int_requests)} pending internal requests")
        try:
            req = self.__pending_int_requests.pop()
            self.__process_resolve_to(req)
        except IndexError:
            pass
        except:
            self.console.print_exception(show_locals=True)

        self.__flush_responses()

    def __flush_responses(self):
        """Send all responses to requests in the queue"""
        self.get_logger().debug(f"{len(self.__response_msgs)} messages to send")
        try:
            sorted = [(m.header.stamp.sec, m.header.stamp.nanosec, m) for m in self.__response_msgs]
            heapify(sorted)
            for (_, _, m) in sorted:
                self.__response_publisher.publish(m)
                self.get_logger().debug(f".. publishing {m}")
            self.__response_msgs.clear()
        except IndexError:
            pass
        except:
            self.console.print_exception(show_locals=True)


    def __process_transition_request(self, request: GoalRequestMsg[T]):
        response = GoalState()
        response.header.frame_id = self.name
        response.header.stamp = self.get_clock().now().to_msg()
        response.goal_id = request.request.goal_id
        response.state = GoalState.UNKNOWN

        if not self.active:
            self.get_logger().error(f"Actor not activated! Cannot accept request... current state: {self.state_label} [{self.state}]")
            self.__response_msgs.append(response)
            return

        state : int = self.get_goal_state(request.request.goal_id)
        step : int = request.request.step
        if step == GoalRequest.FORMULATE:
            self.get_logger().info(f"Received FORMULATE request {request.request}")
            if state != GoalState.UNKNOWN:
                response.state = state
                self.get_logger().error(f"Goal already known! {response}")
                self.__response_msgs.append(response)
            else:
                # Formulating
                self.__formulate(request.request.goal_id, request.goal)
                response.state = self.get_goal_state(request.request.goal_id)
                self.__response_msgs.append(response)

        elif step == GoalRequest.SELECT:
            self.get_logger().info(f"Received SELECT request {request.request}")
            if state != GoalState.FORMULATED:
                response.state = self.get_goal_state(request.request.goal_id)
                self.get_logger().error(f"Goal cannot be selected! {response}")
                self.__response_msgs.append(response)
                return
            # Selecting
            if self.__select_unique:
                ongoing_goals = [i for i, s in self.__goal_states.items()
                                 if GoalState.SELECTED <= s <= GoalState.EVALUATED]
                if len(ongoing_goals) > 0:
                    self.get_logger().error(f"Actor has already selected {len(ongoing_goals)} goals")
                    response.state = self.get_goal_state(request.request.goal_id)
                    self.__response_msgs.append(response)
                    return

            self.__select(request.request.goal_id)
            response.state = self.get_goal_state(request.request.goal_id)
            self.__response_msgs.append(response)

        elif step == GoalRequest.EXPAND:
            self.get_logger().info(f"Received EXPAND request {request.request}")
            if state != GoalState.SELECTED:
                response.state = self.get_goal_state(request.request.goal_id)
                self.get_logger().error(f"Goal cannot be expanded! {response}")
                self.__response_msgs.append(response)
                return
            # Expanding
            if self.__expand(request.request.goal_id):
                for plan_id, p in self.__expansions[request.request.goal_id].items():
                    plan, cost = p
                    self.get_logger().debug(f"new plan {plan_id}")
                    exp = Expansion(plan_id=plan_id,
                                    plan=plan.to_msg(),
                                    costs=(cost if isinstance(cost, list) else [cost]))
                    self.get_logger().debug(f"new expansion {exp}")
                    response.expansions.append(exp)
                    self.get_logger().debug(f"Adding new plan {plan_id} to expansions of {request.request.goal_id}")

            response.state = self.get_goal_state(request.request.goal_id)
            self.__response_msgs.append(response)

        elif step == GoalRequest.COMMIT:
            self.get_logger().info(f"Received COMMIT request {request.request}")
            if state != GoalState.EXPANDED:
                response.state = self.get_goal_state(request.request.goal_id)
                self.get_logger().error(f"Goal cannot be committed! {response}")
                self.__response_msgs.append(response)
                return
            # Committing        
            if self.__commit_unique:
                ongoing_goals = [i for i, s in self.__goal_states.items()
                                 if GoalState.COMMITTED <= s <= GoalState.EVALUATED]
                if len(ongoing_goals) > 0:
                    self.get_logger().error(f"Actor has already committed {len(ongoing_goals)} goals")
                    response.state = self.get_goal_state(request.request.goal_id)
                    self.__response_msgs.append(response)
                    return

            gid = request.request.goal_id
            pid = request.request.plan_id
            if pid not in self.__expansions[gid]:
                self.get_logger().warn(f"Goal {gid} commitment failed! plan {pid} unknown")
                response.state = self.get_goal_state(request.request.goal_id)
                self.__response_msgs.append(response)
                return

            self.__commit(request.request.goal_id)
            response.state = self.get_goal_state(request.request.goal_id)
            response.plan_id = pid
            response.expansions.append(Expansion(plan_id=pid, costs=[], plan=self.get_plan(gid).to_msg()))
            self.__response_msgs.append(response)

        elif step == GoalRequest.DISPATCH:
            self.get_logger().info(f"Received DISPATCH request {request.request}")
            if state != GoalState.COMMITTED:
                response.state = self.get_goal_state(request.request.goal_id)
                self.get_logger().error(f"Goal cannot be dispatched! {response}")
                self.__response_msgs.append(response)

            else:
                # Dispatching
                self.__dispatch(request.request.goal_id)
                response.state = self.get_goal_state(request.request.goal_id)
                self.__response_msgs.append(response)

        elif step == GoalRequest.DROP:
            self.get_logger().info(f"Received DROP request {request.request}")
            if state > GoalState.UNKNOWN and state < GoalState.FINISHED:
                # Dropping
                self.__drop(request.request.goal_id)
                response.state = self.get_goal_state(request.request.goal_id)
                self.__response_msgs.append(response)

            else:
                response.state = self.get_goal_state(request.request.goal_id)
                self.get_logger().error(f"Goal cannot be dropped! {response}")
                self.__response_msgs.append(response)

        else:
            self.get_logger().warn(f"Received unknown request type {request.request}")

    def __process_resolve_to(self, request: GoalRequestMsg[T]):
        """Resolve to a previous state"""

        auto_resolve = False  # The actor triggers the lifecycle traversal by itself

        response = GoalState()
        response.header.frame_id = self.name
        response.header.stamp = self.get_clock().now().to_msg()
        response.goal_id = request.request.goal_id
        response.state = GoalState.UNKNOWN

        state: int = self.get_goal_state(request.request.goal_id)  # The current goal state
        step: int = request.request.step  # The target goal state

        # Check invalid resolution requests
        if not self.active:
            self.get_logger().error(
                f"Actor not activated! Cannot accept request... current state: {self.state_label} [{self.state}]")
            self.__response_msgs.append(response)
            return

        if state == step:
            self.get_logger().debug(f"Goal {request.goal} cannot resolve to {step} from state {state}")
            return

        if state in [GoalState.FINISHED, GoalState.DROPPED]:
            self.get_logger().error(
                f"Goal {request.goal} cannot resolve to state {step} from a terminal state {state}")
            return

        if state == GoalState.UNKNOWN:
            self.get_logger().error(
                f"Goal {request.goal} cannot resolve to state {step} from state {state}")
            return

        # If the goal must be resolved to a state *step* preceeding DISPATCHED
        if state > step and step < GoalState.DISPATCHED:
            self.get_logger().info(f"Dropping {request.request.goal_id} subgoals")
            self._drop_subgoals(request.request.goal_id)

        # Reform
        if step == GoalRequest.FORMULATE:
            self.get_logger().info(f"Processing REFORM request {request.request}")

            self.__formulate(request.request.goal_id, request.goal)
            response.state = self.get_goal_state(request.request.goal_id)
            self.__response_msgs.append(response)

            if response.state == GoalState.FORMULATED:
                # Trigger the next transition
                req: GoalRequestMsg = self.__request_msg()
                req.request.header.frame_id = self.get_name()
                req.request.header.stamp = self.get_clock().now().to_msg()
                req.request.goal_id = request.request.goal_id
                req.request.step = GoalRequest.SELECT
                if auto_resolve:
                    self._request_internal_transition(req)  # Re-traverse lifecycle

        # Defer
        elif step == GoalRequest.SELECT:
            self.get_logger().info(f"Processing DEFER request {request.request}")

            self.__select(request.request.goal_id)
            response.state = self.get_goal_state(request.request.goal_id)
            self.__response_msgs.append(response)

            if response.state == GoalState.SELECTED:
                # Trigger the next transition
                req: GoalRequestMsg = self.__request_msg()
                req.request.header.frame_id = self.get_name()
                req.request.header.stamp = self.get_clock().now().to_msg()
                req.request.goal_id = request.request.goal_id
                req.request.step = GoalRequest.EXPAND
                if auto_resolve:
                    self._request_internal_transition(req)  # Re-traverse lifecycle

        # Replan
        elif step == GoalRequest.EXPAND:
            self.get_logger().info(f"Processing REPLAN request {request.request}")

            if self.__expand(request.request.goal_id):
                for plan_id, p in self.__expansions[request.request.goal_id].items():
                    plan, cost = p
                    self.get_logger().debug(f"new plan {plan_id}")
                    exp = Expansion(plan_id=plan_id,
                                    plan=plan.to_msg(),
                                    costs=(cost if isinstance(cost, list) else [cost]))
                    self.get_logger().debug(f"new expansion {exp}")
                    response.expansions.append(exp)
                    self.get_logger().debug(f"Adding new plan {plan_id} to expansions of {request.request.goal_id}")

            response.state = self.get_goal_state(request.request.goal_id)
            self.__response_msgs.append(response)

            if response.state == GoalState.EXPANDED:
                # Trigger the next transition
                req: GoalRequestMsg = self.__request_msg()
                req.request.header.frame_id = self.get_name()
                req.request.header.stamp = self.get_clock().now().to_msg()
                req.request.goal_id = request.request.goal_id
                req.request.plan_id = response.expansions[0].plan_id
                req.request.step = GoalRequest.COMMIT
                if auto_resolve:
                    self._request_internal_transition(req)  # Re-traverse lifecycle

        # Repair
        elif step == GoalRequest.COMMIT:
            self.get_logger().info(f"Processing REPAIR request {request.request}")
            gid = request.request.goal_id
            pid = request.request.plan_id
            if pid and pid not in self.__expansions[gid]:
                self.get_logger().warn(f"Goal {gid} commitment failed! plan {pid} unknown")
                response.state = self.get_goal_state(request.request.goal_id)
                self.__response_msgs.append(response)

            if self.__commit(request.request.goal_id):
                response.state = self.get_goal_state(request.request.goal_id)
                response.plan_id = pid
                response.expansions.append(Expansion(plan_id=pid, costs=[], plan=self.get_plan(gid).to_msg()))
                self.__response_msgs.append(response)

                if response.state == GoalState.COMMITTED:
                    # Trigger the next transition
                    req: GoalRequestMsg = self.__request_msg()
                    req.request.header.frame_id = self.get_name()
                    req.request.header.stamp = self.get_clock().now().to_msg()
                    req.request.goal_id = request.request.goal_id
                    req.request.step = GoalRequest.DISPATCH
                    if auto_resolve:
                        self._request_internal_transition(req)  # Re-traverse lifecycle
            else:
                # No more commitment options -> DROP
                self.get_logger().warn(f"Goal {gid} commitment failed! DROP")
                self.__drop(request.request.goal_id)
                response.state = self.get_goal_state(request.request.goal_id)
                self.__response_msgs.append(response)

        # Continue
        elif step == GoalRequest.DISPATCH:
            self.get_logger().info(f"Processing CONTINUE request {request.request}")
            self._continue(request.request.goal_id)

        elif step == GoalRequest.DROP:
            self.get_logger().info(f"Received DROP request {request.request}")
            if GoalState.UNKNOWN < state < GoalState.FINISHED:
                # Dropping
                self.__drop(request.request.goal_id)
                response.state = self.get_goal_state(request.request.goal_id)
                self.__response_msgs.append(response)

            else:
                response.state = self.get_goal_state(request.request.goal_id)
                self.get_logger().error(f"Goal cannot be dropped! {response}")
                self.__response_msgs.append(response)

        else:
            self.get_logger().warn(f"Received unknown request type {request.request}")

    def _drop_subgoals(self, goal_id: UID) -> bool:
        """Drop dispatched subgoals"""

        if goal_id in self.__committed_plan:
            _, plan = self.__committed_plan[goal_id]
            # Iterate over the committed plan tasks and drop sub-goals if needed
            result = True
            for (i, t) in plan.get_tasks():
                if (t.state == GoalState.UNKNOWN
                        or t.state == GoalState.DROPPED
                        or t.state == GoalState.FINISHED):
                    continue
                client = self.get_actor_client(t.actor)
                self.get_logger().info(f"Dropping sub-goal {t.id} of goal {goal_id}")
                result = client.drop(t.id) and result

            return result

        else:
            # If there is nothing to do, then the operation is successful
            return True


    @final
    def get_goal(self, goal_id: UID) -> MsgType:
        """ Get a goal stored in memory by id.

        :param goal_id: Goal ID.
        :return: The goal data if it exists, None otherwise
        """
        return self.__goals.get(goal_id, None)

    @final
    def get_goal_state(self, goal_id: UID) -> int:
        """ Get a goal state stored in memory by id.

        :param goal_id: Goal ID.
        :return: The goal state if it exists, UNKNWON otherwise
        """
        return self.__goal_states.get(goal_id, GoalState.UNKNOWN)

    ###------------------------ FORMULATE ---###
    def __formulate(self, gid: UID, goal: T
            #report: Optional[DispatchReport] = None, event: Optional[Event] = None
            ) -> bool:
        self.__goals[gid] = goal
        self.__goal_states[gid] = GoalState.FORMULATED
        self.get_logger().info(f"Goal {gid} FORMULATED")
        return True

    ###------------------------ SELECT ---###
    def __select(self, gid: UID
            #report: Optional[DispatchReport] = None,
            #event: Optional[Event] = None
            ) -> bool:
        goal : T = self.__goals[gid]
        self.get_logger().debug(f"Calling SELECT strategy for goal {gid}")
        try:
            selected = self.select(gid, goal)#, report=report, event=event)
        except Exception as ex:
            self.get_logger().error(f"select raised exception {ex}: {traceback.format_exc()}")
            selected = False

        if selected:
            self.__goal_states[gid] = GoalState.SELECTED
            self.get_logger().info(f"Goal {gid} SELECTED")
            return True
        else:
            self.get_logger().warn(f"Goal {gid} selection refused!")
            return False

    @abstractmethod
    def select(self, goal_id: UID, goal: MsgType
            #,report: Optional[DispatchReport],
            #event: Optional[Event]
            ) -> bool:
        """ **Select** step.

        :param goal_id: goal ID whose selection is requested.
        :param goal: goal structure.
        :param report: in case select is called following a **Reform**, the dispatch report causing the reform is attached.
        :param event: in case select is called following a **Reform**, the event causing the reform is attached.

        If this method returns True, the goal will be in the *SELECTED* state;
        Otherwise the selection is rejected and the goal stays *FORMULATED*.
        """
        return True

    ###------------------------ EXPAND ---###
    def __expand(self, gid: UID
            #,
            #report: Optional[DispatchReport] = None,
            #event: Optional[Event] = None
            ) -> bool:

        goal : T = self.__goals[gid]
        self.get_logger().debug(f"Calling EXPAND strategy for goal {gid}")
        self.get_logger().debug(f"deleting previous {len(self.__expansions[gid])} plans")
        del self.__expansions[gid]

        try:
            expanded = self.expand(gid, goal)#, report=report, event=event)
        except Exception as ex:
            self.get_logger().error(f"expand raised exception {ex}: {traceback.format_exc()}")
            expanded = False

        if expanded:
            self.__goal_states[gid] = GoalState.EXPANDED
            self.get_logger().info(f"Goal {gid} EXPANDED")
            self.get_logger().debug(f"Goal {gid} has {len(self.__expansions[gid])} expansions")
            return True
        else:
            self.get_logger().warn(f"Goal {gid} expansion failed!")
            return False

    @final
    def add_plan(self, gid: UID, plan: Plan, cost: Union[int, List[int]]) -> UID:
        """ Add a new plan to the Actor memory.

        :param gid: goal ID.
        :param plan: the plan.
        :param cost: Cost of the plan, as either an int value or a list of costs.
        :return: the ID attached to the plan.
        """
        pid = UID()
        self.__expansions[gid][pid] = (plan, cost)
        self.get_logger().debug(f"Adding new plan {pid}")
        return pid

    @abstractmethod
    def expand(self, goal_id: UID, goal: MsgType
            #, report: Optional[DispatchReport],
            #event: Optional[Event]
            ) -> bool:
        """ **Expand** step.

        :param goal_id: goal ID whose expansion is requested.
        :param goal: goal structure.
        :param report: in case expand is called following a **Replan**, the dispatch report causing the replan is attached.
        :param event: in case expand is called following a **Replan**, the event causing the replan is attached.

        If this method returns True, the goal will be in the *EXPANDED* state;
        Otherwise the expansion is rejected and the goal stays *SELECTED*.
        """
        return False

    ###------------------------ COMMIT ---###
    def get_plan(self, gid: UID) -> Plan:
        """Get the commited plan.

        :param gid: the goal ID.
        :return: the commited plan for this goal ID.
        """
        return self.__committed_plan[gid][1]

    def __commit(self, gid: UID, pid: Optional[UID] = None
            #,
            #report: Optional[DispatchReport] = None,
            #event: Optional[Event] = None
            ) -> bool:
        goal = self.__goals[gid]
        self.get_logger().debug(f"Calling COMMIT strategy for goal {gid}")
        if pid is None:
            try:
                # we look for an available plan, according to the replan choice strategy; should be called only on the *replan* strategy
                if self.model.replan_choice == ModuleInfo.REPLAN_RANDOM:
                    pid = next(iter(self.__expansions[gid]))
                elif self.model.replan_choice == ModuleInfo.REPLAN_MIN:
                    pid = min(self.__expansions[gid], key=lambda k: self.__expansions[gid][k][1])
                elif self.model.replan_choice == ModuleInfo.REPLAN_MAX:
                    pid = max(self.__expansions[gid], key=lambda k: self.__expansions[gid][k][1])
                self.get_logger().debug(f"-- {self.model.replan_choice} choosed plan {pid}")
            except Exception as ex:
                self.get_logger().error(f"Failed to choose an expansion {ex}: {traceback.format_exc()}")
                return False
        try:
            commited_plan_with_score = self.__expansions[gid][pid]
            self.__committed_plan[gid] = (pid, commited_plan_with_score[0])
            if self.commit(gid, goal, self.__committed_plan[gid][1]):#, report=report, event=event):
                self.get_logger().info(f"Goal {gid} COMMITTED")
                self.__goal_states[gid] = GoalState.COMMITTED
                return True
            else:
                self.get_logger().warn(f"Goal {gid} commitment failed!")
                return False
        except Exception as ex:
            self.get_logger().error(f"commit raised exception {ex}: {traceback.format_exc()}")
            return False

    @abstractmethod
    def commit(self, goal_id: UID, goal: MsgType, plan: Plan
            #,
            #report: Optional[DispatchReport],
            #event: Optional[Event]
            ) -> bool:
        """ **Commit** step.

        :param goal_id: goal ID whose selection is requested.
        :param goal: goal structure.
        :param plan: plan to commit to.
        :param report: in case commit is called following a **Repair**, the dispatch report causing the repair is attached.
        :param event: in case commit is called following a **Repair**, the event causing the repair is attached.

        If this method returns True, the goal will be in the *COMMITTED* state;
        Otherwise the commitment is rejected and the goal stays *EXPANDED*.
        """
        pass

    ###------------------------ DISPATCH ---###
    def __dispatch(self, gid: UID
                    #,
                    #        report: Optional[DispatchReport] = None,
                    #        event: Optional[Event] = None
                    ) -> bool:
        """Dispatch a goal"""
        goal: T = self.__goals[gid]
        self.get_logger().debug(f"Calling DISPATCH strategy for goal {gid}")
        try:
            if self.dispatch(gid, goal, self.__committed_plan[gid][1]):
                self.__dispatched_expansions[gid].add(self.__committed_plan[gid][0])
                del self.__expansions[gid][self.__committed_plan[gid][0]]
                self.__goal_states[gid] = GoalState.DISPATCHED
                self.get_logger().info(f"Goal {gid} DISPATCHED")
                return True
            else:
                self.get_logger().error(f"goal {gid} dispatching failed")
                return False

        except Exception as ex:
            self.get_logger().error(f"dispatch raised exception {ex}: {traceback.format_exc()}")
            return False

    @abstractmethod
    def dispatch(self, goal_id: UID, goal: T, plan: Plan) -> bool:
        """ **Dispatch** step.

        :param goal_id: goal ID whose dispatching is requested.
        :param goal: goal structure.
        :param plan: the plan to dispatch.

        If this method returns True, the goal will be in the *DISPATCHED* state;
        Otherwise the dispatching is rejected and the goal stays *COMMITTED*.
        """
        pass

    ###------------------------ DROP ---###
    def __drop_goal(self, gid: UID):
        """Remove a goal from memory and set it in state DROPPED"""

        self.__expansions.pop(gid, None)
        self.__committed_plan.pop(gid, None)
        if self.__goal_states[gid] != GoalState.FINISHED:
            self.__goal_states[gid] = GoalState.DROPPED

    def __drop(self, gid: UID) -> bool:
        try:
            dropped = self.drop(gid)
        except Exception as ex:
            self.get_logger().error(f"drop raised exception {ex}: {traceback.format_exc()}")
            dropped = False

        if not dropped:
            self.get_logger().error(f"Dropping goal {gid} impossible!")
            return False

        else:
            self.get_logger().debug(f"Goal {gid} was in state {self.__goal_states[gid]}")
            self.__drop_goal(gid)
            self.get_logger().info(f"Goal {gid} DROPPED")
            # TODO: self.__terminate_monitoring(gid)        
            return True

    @abstractmethod
    def drop(self, goal_id: UID) -> bool:
        """ Function called when dropping a goal.

        Note that the dropping the goal itself is managed by the Actor.
        Here, potentially active subgoals have to be dropped.

        :param goal_id: the goal to drop.
        :return: must return True if dropping subgoals can be effective.
        """
        pass

    ###------------------------ MONITORING ---###
    @final
    def get_dispatched_goals(self) -> List[UID]:
        """Get the list of goals currently dispatched."""
        return [gid for gid, state in self.__goal_states.items() if state == GoalState.DISPATCHED]

    @abstractmethod
    def monitor(self, goal_id: UID):
        """ **Monitor** hook.

        :param goal_id: goal ID currently dispatched to monitor.
        """
        pass


    @final
    def finish(self, gid: UID) -> bool:
        """ Indicate that a goal has finished.

        The goal state is then changed to FINISHED, and reported to the parent actor.

        :param gid: Goal ID.
        :returns: False is the goal id is unknown or the Finish step not allowed; otherwise True
        """
        if gid not in self.__goal_states:
            self.get_logger().error(f"Goal {gid} unknown!")
            return False

        state = self.__goal_states[gid]
        if state not in [GoalState.EVALUATED, GoalState.DISPATCHED]:
            self.get_logger().error(f"Trying to finish goal {gid} in state {state}!")
            return False

        self.__goal_states[gid] = GoalState.FINISHED
        self.get_logger().info(f'Goal {gid} FINISHED')
        response = GoalState()
        response.header.frame_id = self.name
        response.header.stamp = self.get_clock().now().to_msg()
        response.goal_id = gid
        response.state = self.__goal_states[gid]
        self.__response_msgs.append(response)
        return True

    @final
    def _drop(self, gid: UID) -> bool:
        """ Indicate that a goal has to be dropped.

        :param gid: Goal ID.
        :returns: False is the goal id is unknown or the DROP step not allowed; otherwise True
        """
        if gid not in self.__goal_states:
            self.get_logger().error(f"Goal {gid} unknown!")
            return False

        self.get_logger().info(f"DROP goal {gid}")
        req: GoalRequestMsg = self.__request_msg()
        req.request.header.frame_id = self.get_name()
        req.request.header.stamp = self.get_clock().now().to_msg()
        req.request.goal_id = gid
        req.request.step = GoalRequest.DROP
        self._request_internal_transition(req)

        return True

    @final
    def _continue(self, gid: UID) -> bool:
        """Continue the goal execution.

        Changes the state from EVALUATED to DISPATCHED

        :param gid: Goal ID.
        :returns: False is the goal id is unknown or not in the EVALUATED status; otherwise True
        """
        if gid not in self.__goal_states:
            self.get_logger().error(f"Goal {gid} unknown!")
            return False

        #Continue does not require special action unlike the other resolve-to actions
        self.get_logger().info(f"CONTINUE goal {gid}")
        self.__goal_states[gid] = GoalState.DISPATCHED
        self.get_logger().info(f'Goal {gid} DISPATCHED')
        response = GoalState()
        response.header.frame_id = self.name
        response.header.stamp = self.get_clock().now().to_msg()
        response.goal_id = gid
        response.state = self.__goal_states[gid]
        self.__response_msgs.append(response)

        return True

    def _repair_request(self, gid: UID) -> bool:
        """ Indicate that a goal has to be repaired.

        Issue an internal COMMIT request.

        :param gid: Goal ID.
        :returns: False is the goal id is unknown or the COMMITTED step not allowed; otherwise True
        """

        if gid not in self.__goal_states:
            self.get_logger().error(f"Goal {gid} unknown!")
            return False

        self.get_logger().info(f"REPAIR goal {gid}")
        req: GoalRequestMsg = self.__request_msg()
        req.request.header.frame_id = self.get_name()
        req.request.header.stamp = self.get_clock().now().to_msg()
        req.request.goal_id = gid
        req.request.plan_id = ''  # The resolve-to process decides which plan_id must come up next.
        req.request.step = GoalRequest.COMMIT

        self._request_internal_transition(req)

        return True

    def _replan_request(self, gid: UID) -> bool:
        """ Indicate that a goal has to be replaned.

        Issue an internal EXPAND request.

        :param gid: Goal ID.
        :returns: False is the goal id is unknown or the EXPANDED step not allowed; otherwise True
        """

        if gid not in self.__goal_states:
            self.get_logger().error(f"Goal {gid} unknown!")
            return False

        self.get_logger().info(f"REPLAN goal {gid}")
        req: GoalRequestMsg = self.__request_msg()
        req.request.header.frame_id = self.get_name()
        req.request.header.stamp = self.get_clock().now().to_msg()
        req.request.goal_id = gid
        req.request.step = GoalRequest.EXPAND

        self._request_internal_transition(req)

        return True

    def _defer_request(self, gid: UID) -> bool:
        """Indicate that a goal has to be deferred.

        Issue an internal SELECT request.

        :param gid: Goal ID.
        :returns: False is the goal id is unknown or the SELECTED step not allowed; otherwise True
        """

        if gid not in self.__goal_states:
            self.get_logger().error(f"Goal {gid} unknown!")
            return False

        self.get_logger().info(f"DEFER goal {gid}")
        req: GoalRequestMsg = self.__request_msg()
        req.request.header.frame_id = self.get_name()
        req.request.header.stamp = self.get_clock().now().to_msg()
        req.request.goal_id = gid
        req.request.step = GoalRequest.SELECT

        self._request_internal_transition(req)

        return True

    def _reform_request(self, gid: UID) -> bool:
        """Indicate that a goal has to be reformed.

        Issue an internal FORMULATE request.

        :param gid: Goal ID.
        :returns: False is the goal id is unknown or the FORMULATED step not allowed; otherwise True
        """

        if gid not in self.__goal_states:
            self.get_logger().error(f"Goal {gid} unknown!")
            return False

        self.get_logger().info(f"REFORM goal {gid}")
        req: GoalRequestMsg = self.__request_msg()
        req.request.header.frame_id = self.get_name()
        req.request.header.stamp = self.get_clock().now().to_msg()
        req.request.goal_id = gid
        req.request.step = GoalRequest.FORMULATE

        self._request_internal_transition(req)

        return True

    ###------------------------- PROCESS/EVALUATE ---###
    def __process_event(self, name: str, event: Event):
        self.get_logger().info(f"Processing event {name}: {event}")
        strategy = self.__event_strategies[name]
        self.get_logger().info(f"-- strategy {strategy.name}")
        if strategy == EventReaction.PROCESS:
            self.process(name, event)
        elif event.value:
            if strategy == EventReaction.REFORM:
                for gid in self.__goals:
                    self._reform_request(gid)
            elif strategy == EventReaction.REFORM_DISPATCHED_GOALS:
                for gid in self.get_dispatched_goals():
                    self._reform_request(gid)
            elif strategy == EventReaction.REPLAN:
                for gid in self.__goals:
                    self._replan_request(gid)
            elif strategy == EventReaction.REPLAN_DISPATCHED_GOALS:
                for gid in self.get_dispatched_goals():
                    self._replan_request(gid)
            elif strategy == EventReaction.REPAIR:
                for gid in self.__goals:
                    self._repair_request(gid)
            elif strategy == EventReaction.REPAIR_DISPATCHED_GOALS:
                for gid in self.get_dispatched_goals():
                    self._repair_request(gid)
            elif strategy == EventReaction.DEFER:
                for gid in self.__goals:
                    self._defer_request(gid)
            elif strategy == EventReaction.DEFER_DISPATCHED_GOALS:
                for gid in self.get_dispatched_goals():
                    self._defer_request(gid)
            elif strategy == EventReaction.CONTINUE:
                for gid in self.__goals:
                    self._continue(gid)
            elif strategy == EventReaction.CONTINUE_DISPATCHED_GOALS:
                for gid in self.get_dispatched_goals():
                    self._continue(gid)
            

    def process(self, name: str, event: Event):
        """ Process an incoming event.

        Default event processing does nothing.

        :param name: the event name.
        :param event: the received event value.
        """
        pass

    @final
    def __to_evaluated(self, gid: UID, report: DispatchReport):
        """Change status to EVALUATED if possible

        :returns True if the change was successful"""
        self.get_logger().debug(f"Received evaluate request {report} for goal {gid}")
        if gid not in self.__goals:
            self.get_logger().error(f"Goal {gid} unknown!")
            return False

        if self.__goal_states[gid] != GoalState.DISPATCHED:
            self.get_logger().error(f"Goal {gid} not in DISPATCHED state. Current state is {self.__goal_states[gid]}")
            return False

        self.__goal_states[gid] = GoalState.EVALUATED
        self.get_logger().info(f'Goal {gid} EVALUATED')
        response = GoalState()
        response.header.frame_id = self.name
        response.header.stamp = self.get_clock().now().to_msg()
        response.goal_id = gid
        response.state = self.__goal_states[gid]
        self.__response_msgs.append(response)
        return True

    @final
    def __evaluate(self, gid: UID, report: DispatchReport):
        """Call the user-defined evaluate function and manage the result"""

        if self.__goal_states[gid] != GoalState.EVALUATED:
            self.get_logger().error(f"Goal {gid} not in EVALUATED state. Current state is {self.__goals[gid].state}")
            return False

        eval_result = self.evaluate(gid, report)

        if eval_result == ResolveTo.REFORM:  # Reform
            return self._reform_request(gid)
        elif eval_result == ResolveTo.DEFER:  # Defer
            return self._defer_request(gid)
        elif eval_result == ResolveTo.REPLAN:  # Replan
            return self._replan_request(gid)
        elif eval_result == ResolveTo.REPAIR:  # Repair
            return self._repair_request(gid)
        elif eval_result == ResolveTo.DROP:  # Drop
            return self._drop(gid)
        elif eval_result == ResolveTo.CONTINUE:  # Continue
            return self._continue(gid)
        else:
            self.get_logger().error(f"Invalid ResolveTo trigger {eval_result} from 'evaluate'")
            return False

    @abstractmethod
    def evaluate(self, gid: UID, report: DispatchReport) -> int:
        """ Evaluate an event raised when monitoring a goal.

        :param gid: Goal ID.
        :param report: Report about the actor change of state that triggered the evaluation
        :return: a resolving step, as defined in the oara_interfaces.msg.ResolveTo message
        """
        pass
