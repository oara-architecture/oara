from typing import Type, TypeVar, final
from abc import abstractmethod

from oara_interfaces.msg import Event, ModuleInfo, ResolveTo
from ..internal import MsgType, UID
from ..internal.report import DispatchReport
from ..internal.plan import Plan

from .actor import Actor

T = TypeVar('T')

class Controller(Actor[T]):
    """ A ROS2 OARA Controller.
    """
    def __init__(self, name: str, msg: Type[T], **kwargs):
        super().__init__(name, msg, **kwargs)
        self.model.pattern = ModuleInfo.ACTOR_CONTROLLER

    @final
    def expand(self, gid: UID, goal: T,
            #report: Optional[DispatchReport],
            #event: Optional[Event]
            ) -> bool:
        """ Add a dummy plan containing the goal.

        :return: True
        """
        p = Plan()
        p.add_task(0, goal, self.name)
        self.add_plan(gid, p, 0.0)
        return True

    @final
    def commit(self, gid: UID, goal: T, plan: Plan,
            #report: Optional[DispatchReport],
            #event: Optional[Event]
            ) -> bool:
        """ Commit always successful.
        
        :return: True
        """
        return True

    @final
    def process(self, event: Event):
        """ Controller actors cannot react to external events.

        Event processing should be performed by parent actors.
        """
        return

    @final
    def evaluate(self, gid: UID, report: DispatchReport):
        """ Evaluates the event.

        Success event trigger `continue`, all others trigger `reform`.
        """
        if report.status:
           return ResolveTo.CONTINUE
        else:
           return ResolveTo.REFORM

    @abstractmethod
    def dispatch(self, goal_id: UID, goal: T, plan: Plan) -> bool:
        """ **Dispatch** step.

        :param goal_id: goal ID whose selection is requested.
        :param goal: goal structure.
        :param plan: the plan to dispatch.

        If this method returns True, the goal will be in the *DISPATCHED* state;
        Otherwise the dispatching is rejected and the goal stays *COMMITTED*.
        """
        pass

    @abstractmethod
    def monitor(self, goal_id: UID):
        """ Monitor the dispatch of a goal.

        :param goal_id: goal ID.
        """
        pass

    @abstractmethod
    def drop(self, goal_id: UID) -> bool:
        """ Function called when dropping a goal.

        Note that the actual dropping (removing the goal from the actor internal structure) will be 
        done by the actor internals. Here, you can do more specific things to drop a goal 
        (freeing some associated memory, stopping an external process, ...).

        :param goal_id: the goal to drop.
        :return: must return True if dropping can be effective.
        """
        pass
