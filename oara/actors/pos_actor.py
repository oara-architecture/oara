import queue
from typing import Dict, List, Set, Optional, Type, TypeVar
from functools import partial
from collections import defaultdict

import rclpy

from oara_interfaces.msg import Event, GoalState, ModuleInfo

from ..internal import MsgType, UID
from ..internal.plan import Plan
from ..internal.task import Task
from ..internal.report import DispatchReport

from .actor import Actor

T = TypeVar('T')


class POSActor(Actor[T]):
    def __init__(self, name: str, task: Type[T], select_unique: bool = False, commit_unique: bool = False):
        super().__init__(name, task, select_unique=select_unique, commit_unique=commit_unique)
        self.model.pattern = ModuleInfo.ACTOR_POS

        self.__plans : Dict[UID, Plan] = dict()
        self.__active_tasks : Dict[UID, Set[int]] = defaultdict(set)
        self.__pending_tasks : Dict[UID, Set[int]] = defaultdict(set)

    def commit(self, goal_id: UID, goal: T, plan: Plan
               #, report: Optional[DispatchReport], event: Optional[Event]
               ) -> bool:
        self.__plans[goal_id] = plan
        self.__plans[goal_id].freeze()
        self.__pending_tasks[goal_id] = {i for i, _ in plan.get_tasks()}
        self.get_logger().info(f"committing {len(self.__pending_tasks[goal_id])} pending tasks")
        return True

    def __dispatch_task(self, goal_id: UID, index: int, task: Task) -> bool:
        self.get_logger().info(f"Task {index} has no predecessor: dispatching")
        self.__active_tasks[goal_id].add(index)
        self.__pending_tasks[goal_id].remove(index)
        client = self.get_actor_client(task.actor)

        if not client.traverse_lifecycle(task):
            self.get_logger().error(f"Error executing task {task}")
            return False
        return True

    def dispatch(self, goal_id: UID, g: T, p: Plan) -> bool:
        plan = self.__plans[goal_id]
        executable_tasks : List[int] = []
        for i in self.__pending_tasks[goal_id]:
            preds : List[bool] = [plan.get_task(j).state == GoalState.FINISHED for j in plan.predecessors(i)]
            if all(preds):
                executable_tasks.append(i)
 
        for i in executable_tasks:
            t = plan.get_task(i)
            if not self.__dispatch_task(goal_id, i, t):
                self.__active_tasks[goal_id].remove(i)
                self.request_evaluate(goal_id, DispatchReport(t.actor, False, t.state))
                return False

        return True

    def monitor(self, goal_id: UID):
        self.get_logger().debug(f"active tasks: {self.__active_tasks[goal_id]}")
        self.get_logger().debug(f"pending tasks: {self.__pending_tasks[goal_id]}")
        if len(self.__active_tasks[goal_id]) == 0:
            if len(self.__pending_tasks[goal_id]) == 0:
                self.get_logger().info("No more active tasks")
                self.get_logger().info("Plan terminated")
                self.finish(goal_id)
                return
            else:
                self.dispatch(goal_id, None, None)
                return

        for index in list(self.__active_tasks[goal_id]):
            task = self.__plans[goal_id].get_task(index)
            client = self.get_actor_client(task.actor)

            last_state = task.state
            self.get_logger().debug(
                f"Task {task} reported [{task.state}]")

            if client.process_event(task.id):
                task.state = client.get_state(task.id).state
                if task.state < last_state and task.state in [GoalState.SELECTED, GoalState.EXPANDED,
                                                              GoalState.COMMITTED]:
                    # The goal has been reverted to a previous state
                    self.get_logger().debug(
                        f"The goal has been resolved to a previous state {last_state} -> {task.state}."
                        f" Request evaluation")
                    # TODO: retry?
                    client.traverse_lifecycle(task)

                if task.state < last_state and task.state == GoalState.FORMULATED:
                    # The goal has been reverted to FORMULATED indicating that
                    # the sub-goal can not be reached
                    self.get_logger().info(f"Task {task} resolved to FORMULATED [{task.state}]")
                    self.request_evaluate(goal_id, DispatchReport(task.actor, True, task.state))

                # elif task.state == GoalState.DISPATCHED or task.state == GoalState.EVALUATED:
                #     processed = client.process_event(task.id)
                #     self.get_logger().debug(
                #         f"Process event {task} result: {processed}")

                elif task.state == GoalState.FINISHED:
                    self.get_logger().info(f"Task {task} finished [{task.state}]")
                    self.__active_tasks[goal_id].remove(index)
                    self.request_evaluate(goal_id, DispatchReport(task.actor, True, task.state))

                elif task.state == GoalState.DROPPED:
                    self.get_logger().info(f"Task {task} dropped [{task.state}]")
                    self.__active_tasks[goal_id].remove(index)
                    self.request_evaluate(goal_id, DispatchReport(task.actor, True, task.state))

                elif task.state == GoalState.FORMULATED:
                    self.get_logger().info(f"Task {task} reported [{task.state}]: evaluate report")
                    #self.create_task(self.evaluate, goal_id, DispatchReport(task.actor, True, task.state))
                else:
                    pass

    def drop(self, goal_id: UID) -> bool:
        if goal_id not in self.__plans:
            return True

        result : bool = True
        for (i, t) in self.__plans[goal_id].get_tasks():
            if (t.state == GoalState.UNKNOWN
                    or t.state == GoalState.DROPPED
                    or t.state == GoalState.FINISHED):
                continue
                
            client = self.get_actor_client(t.actor)
            result = client.drop(t.id) and result

        return result