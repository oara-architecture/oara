from functools import partial
import threading
from typing import TypeVar

from .controller import Controller
from ..internal import MsgType, UID
from ..cli.cli import CLI

T = TypeVar('T')

class PrintController(Controller[T], CLI):
    def __init__(self, msg: T, sleep: float = 1.0):
        Controller.__init__(self, "print_controller", msg, select_unique=False)
        CLI.__init__(self)
        self.get_logger().info(f"PrintController for task {msg.__name__}")
        self.__sleep = sleep
        self.__goal = None
        self.__event = threading.Event()

    def select(self, gid: UID, goal: MsgType):
        return True

    def dispatch(self, goal_id: UID,
                goal: MsgType, plan: MsgType) -> bool:
        self.get_logger().info(f"-- {goal_id} --")
        self.__goal = goal
        self.__event.clear()
        self.__timer = self.create_timer(self.__sleep, partial(self.__timer_cb, goal_id))
        return True

    def __timer_cb(self, gid: UID):
        self.print_message(self.__goal)
        self.get_logger().info(f"-- finishing {gid} --")
        self.__timer.cancel()
        self.__event.set()

    def drop(self, gid: UID):
        try:
            self.__timer.cancel()
        except:
            pass
        return True

    def monitor(self, goal_id: UID):
        if self.__event.is_set():
            self.finish(goal_id)
