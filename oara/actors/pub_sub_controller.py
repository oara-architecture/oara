import threading
from typing import TypeVar
from oara.internal.report import DispatchReport

from std_msgs.msg import Bool
from .controller import Controller
from ..internal import MsgType, UID
from ..cli.cli import CLI
from ..cli.msg_types import msg_from_name


T = TypeVar('T')

class PubSubController(Controller[T], CLI):
    def __init__(self, msg: T):
        Controller.__init__(self, "pub_sub_controller", msg, select_unique=False)
        CLI.__init__(self)
        self.get_logger().info(f"PubSubController for task {msg.__name__}")

        self.__goal_id = None
        self.__goal_result = None

        goal_type = msg.get_fields_and_field_types()['goal']
        self.__pub_msg_type = msg_from_name(goal_type)

    def on_activate(self, state):
        self.__pub = self.create_publisher(self.__pub_msg_type, 'out', 1)
        self.__sub = self.create_subscription(Bool, 'in', self.__cb, 1)
        return super().on_activate(state)

    def on_deactivate(self, state):
        self.__pub.destroy()
        self.__sub.destroy()
        return super().on_deactivate(state)
    
    def select(self, gid: UID, goal: MsgType):
        return True

    def dispatch(self, goal_id: UID,
                goal: MsgType, plan: MsgType) -> bool:
        self.get_logger().info(f"-- {goal_id} --")
        self.__goal_result = None
        self.__goal_id = goal_id
        self.__pub.publish(goal)
        return True

    def __cb(self, msg: Bool):
        if self.__goal_id is not None:
            self.get_logger().info(f"-- finishing {self.__goal_id}: {msg.data} --")
            self.__goal_result = msg.data

    def drop(self, gid: UID):
        self.__goal_id = None
        return True

    def monitor(self, goal_id: UID):
        if self.__goal_id is not None and self.__goal_result is not None:
            if self.__goal_result:
                self.finish(goal_id)
            else:
                self.request_evaluate(goal_id, DispatchReport('pub_sub', False))
            self.__goal_id = None
