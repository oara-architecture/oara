import queue
from collections import defaultdict
from functools import partial
from typing import Dict, Set, Optional, Tuple, Type, TypeVar

import rclpy
import rclpy.time
import rclpy.timer
from oara_interfaces.msg import Event, GoalState

from rclpy_lifecycle import State
from .actor import Actor
from ..internal import MsgType, CallbackReturn
from ..internal import UID
from ..internal.plan import Plan
from oara_interfaces.msg import GoalState, ModuleInfo
from ..internal.task import Task

from ..internal.report import DispatchReport

T = TypeVar('T')


class TimedPOSActor(Actor[T]):
    """Actor doing temporal & partial order scheduling"""

    def __init__(self, name: str, task: Type[T], select_unique: bool = False, commit_unique: bool = False):
        super().__init__(name, task, select_unique=select_unique, commit_unique=commit_unique)
        self.model.pattern = ModuleInfo.ACTOR_TIMEDPOS
        # Add "execution policy" parameter to set if late tasks are dispatched or not. Assume they are by default
        # The oara.monitoring_period parameters acts as the dispatch time quantum (set during configure)
        self.__time_q: float = 1.0
        self.__plans: Dict[UID, Plan] = dict()
        # __active_tasks keeps track of tasks have been or are waiting to be dispatched
        self.__active_tasks: Dict[UID, Set[int]] = defaultdict(set)
        self.__fb_tasks = queue.SimpleQueue()
        # Keep track of active timers
        self.__delayed_dispatch_timer: Dict[Tuple[UID, int], rclpy.timer.Timer] = dict()

    def on_configure(self, state: State) -> CallbackReturn:
        cb_success = super().on_configure(state)
        if cb_success == CallbackReturn.SUCCESS:
            if self.get_parameter('oara.model.monitoring_period').value is not None:
                self.__time_q = self.get_parameter('oara.model.monitoring_period').value
            return CallbackReturn.SUCCESS
        else:
            return cb_success

    def commit(self, goal_id: UID, goal: T, plan: Plan
               # , report: Optional[DispatchReport], event: Optional[Event]
               ) -> bool:
        self.__plans[goal_id] = plan
        self.__plans[goal_id].freeze()
        return True

    def __result_cb(self, index: int, task: Task, future: rclpy.Future):
        self.get_logger().debug(f"received result for task {index}")
        result = future.result()
        task.state = result.state
        if task.state == GoalState.DROPPED:
            self.get_logger().debug(f"task {task} dropped")
        else:
            self.get_logger().debug(f"stacking completed task {task}")
            self.__fb_tasks.put((index, task))

    def __dispatch_task(self, goal_id: UID, index: int, task: Task) -> bool:
        now = self.get_clock().now()
        dispatch_error = (now - task.dispatch_time).nanoseconds / rclpy.time.CONVERSION_CONSTANT
        if dispatch_error > 0.0:
            self.get_logger().info(f"Dispatching task {index} ({dispatch_error} seconds late)")
        else:
            self.get_logger().info(f"Dispatching task {index} ({abs(dispatch_error)} seconds early)")
        # Remove the timer so it only runs once
        self.__delayed_dispatch_timer[(goal_id, index)].cancel()
        self.destroy_timer(self.__delayed_dispatch_timer[(goal_id, index)])
        del self.__delayed_dispatch_timer[(goal_id, index)]

        client = self.get_actor_client(task.actor)
        if not client.traverse_lifecycle(task):
            self.get_logger().error(f"Error executing task {task}")

        return True

    def dispatch(self, goal_id: UID, goal: MsgType, plan: Plan) -> bool:
        for (i, first_task) in plan.get_tasks():
            if len(plan.predecessors(i)) == 0:
                self.__active_tasks[goal_id].add(i)  # Keep track of the current scheduled task
                # Compute time delay
                waiting_time_nsec = max(0.0, first_task.dispatch_time.nanoseconds - self.get_clock().now().nanoseconds)
                # Delayed dispatch
                wait_time_s = waiting_time_nsec / rclpy.time.CONVERSION_CONSTANT
                self.get_logger().debug(f"Traversing lifecycle of task {i} up to COMMITTED")
                client = self.get_actor_client(first_task.actor)
                if not client.traverse_lifecycle(first_task, GoalState.COMMITTED):
                    self.get_logger().error(f"Error executing task {i}")
                self.get_logger().debug(f"Scheduling task {i} to be DISPATCHED in {wait_time_s} seconds")
                self.__delayed_dispatch_timer[goal_id, i] = self.create_timer(
                    wait_time_s, partial(self.__dispatch_task, goal_id, i,
                                         first_task))
        # Dispatching always succeeds because the dispatch of the first task is always delayed
        return True

    def __dispatch_step(self, goal_id: UID, index: int, task: Task):
        """Task dispatching during the monitor phase

        :param task: The task that has just ended"""
        plan = self.__plans[goal_id]
        for i in plan.successors(index):
            self.get_logger().debug(f"checking successor {i}")
            ti = plan.get_task(i)
            if ti.state != GoalState.UNKNOWN:
                self.get_logger().debug(f"task already executing")
                continue

            ready = True
            for j in plan.predecessors(i):
                self.get_logger().debug(f"checking {i}'s predecessor {j}")
                t = plan.get_task(j)
                self.get_logger().debug(f"{j}'s task: {t}")
                if t.state == GoalState.FINISHED or t.state == GoalState.DROPPED:
                    continue
                else:
                    ready = False
                    break

            if ready:
                if i not in self.__active_tasks[goal_id]:  # Final check: task is not been scheduled
                    self.__active_tasks[goal_id].add(i)  # Keep track of the current scheduled task
                    # Compute time delay taking in account the monitoring period
                    # to be as close as possible to the target dispatch time
                    waiting_time_nsec = max(0.0, (ti.dispatch_time.nanoseconds - self.get_clock().now().nanoseconds))
                    if waiting_time_nsec < 0.5 * self.__time_q * rclpy.time.CONVERSION_CONSTANT:
                        waiting_time_nsec = 0.0
                    wait_time_s = waiting_time_nsec / rclpy.time.CONVERSION_CONSTANT
                    self.get_logger().info(f"Scheduling task {i} to run in {wait_time_s} seconds")
                    self.__delayed_dispatch_timer[(goal_id, i)] = \
                        self.create_timer(wait_time_s, partial(self.__dispatch_task, goal_id, i, ti))

    def monitor(self, goal_id: UID):
        self.get_logger().debug(f"active and scheduled tasks: {self.__active_tasks[goal_id]}")
        if len(self.__active_tasks[goal_id]) == 0:
            self.get_logger().info("No more active tasks")
            self.get_logger().info("Plan terminated")
            self.finish(goal_id)
            return

        # if self.__plans[goal_id].is_terminated():
        #     self.get_logger().info("Plan terminated")

        for index in list(self.__active_tasks[goal_id]):
            task = self.__plans[goal_id].get_task(index)

            client = self.get_actor_client(task.actor)
            last_state = task.state
            task.state = client.get_state(task.id).state
            self.get_logger().debug(
                f"Task {task} reported [{task.state}]")

            if task.state < last_state and task.state in [GoalState.SELECTED, GoalState.EXPANDED,
                                                          GoalState.COMMITTED]:
                # The goal has been reverted to a previous state
                self.get_logger().debug(
                    f"The goal has been resolved to a previous state {last_state} -> {task.state}."
                    f" Request evaluation")
                client.traverse_lifecycle(task)

            if task.state < last_state and task.state == GoalState.FORMULATED:
                # The goal has been reverted to FORMULATED indicating that
                # the goal can not be completed
                self.get_logger().info(f"Task {task} resolved to FORMULATED [{task.state}]")
                self.request_evaluate(goal_id, DispatchReport(task.actor, True, task.state))

            elif task.state == GoalState.DISPATCHED or task.state == GoalState.EVALUATED:
                processed = client.process_event(task.id)
                self.get_logger().debug(
                    f"Process event {task} result: {processed}")

            elif task.state == GoalState.FINISHED:
                self.get_logger().info(f"Task {task} finished [{task.state}]")
                self.__active_tasks[goal_id].remove(index)
                self.request_evaluate(goal_id, DispatchReport(task.actor, True, task.state))
                self.__dispatch_step(goal_id, index, task)

            elif task.state == GoalState.DROPPED:
                self.get_logger().info(f"Task {task} dropped [{task.state}]")
                self.__active_tasks[goal_id].remove(index)
                self.request_evaluate(goal_id, DispatchReport(task.actor, True, task.state))

            elif task.state == GoalState.UNKNOWN:
                # Waiting to be dispatched
                continue

            else:
                # self.get_logger().info(f"Task {task} reported [{task.state}]: evaluate report (What's the point of being here?)")
                pass
                # result = client.result(task.id)
                # task.state = result.state
                # if result.success:
                #     self.get_logger().info(f"Task {task} reported [{task.state}]: evaluate report")
                # self.create_task(self.evaluate, goal_id, DispatchReport(task.actor, True, task.state))

    def drop(self, goal_id: UID) -> bool:
        if goal_id not in self.__plans:
            return True

        result: bool = True

        self._discard_dispatch_timers(goal_id)

        for (i, t) in self.__plans[goal_id].get_tasks():
            if (t.state == GoalState.UNKNOWN
                    or t.state == GoalState.DROPPED
                    or t.state == GoalState.FINISHED):
                continue

            client = self.get_actor_client(t.actor)
            result = client.drop(t.id) and result

        return result

    def _discard_dispatch_timers(self, goal_id: UID):
        """Cancel active dispatch timers of goal_id"""
        timers_to_be_deleted = [key for key in self.__delayed_dispatch_timer.keys() if key[0] == goal_id]
        for key in timers_to_be_deleted:
            self.__delayed_dispatch_timer[key].cancel()
            self.destroy_timer(self.__delayed_dispatch_timer[key])
            del self.__delayed_dispatch_timer[key]

    def _repair_request(self, gid: UID) -> bool:
        self._discard_dispatch_timers(gid)
        return super(TimedPOSActor, self)._repair_request(gid)

    def _replan_request(self, gid: UID) -> bool:
        self._discard_dispatch_timers(gid)
        return super(TimedPOSActor, self)._replan_request(gid)

    def _defer_request(self, gid: UID) -> bool:
        self._discard_dispatch_timers(gid)
        return super(TimedPOSActor, self)._defer_request(gid)

    def _reform_request(self, gid: UID) -> bool:
        self._discard_dispatch_timers(gid)
        return super(TimedPOSActor, self)._reform_request(gid)
