import pyfiglet
from rich.console import Console
from rich.syntax import Syntax
from rich.live import Live
from rich.table import Table

from rosidl_runtime_py import message_to_yaml


class CLI:
    def __init__(self) -> None:
        self.console = Console()

    def header(self) -> None:
        header = pyfiglet.figlet_format("OARA", font='isometric1')
        self.console.print(f"[blue]{header}[/blue]")

    def print_message(self, msg) -> None:
        s = str(message_to_yaml(msg))
        syntax = Syntax(s, 'yaml')
        self.console.print(syntax, style="white on black")