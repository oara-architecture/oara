import os
from typing import Tuple
from rclpy.node import Node
from std_msgs.msg import Header
from oara.internal import MsgType
from oara.cli.msg_types import msg_from_name
from oara.internal.data_client import DataClient

class GetData(Node):
    """Get a data from an OARA observer."""
    def __init__(self, node_name: str):
        super().__init__(node_name)

    @property
    def name(self) -> str:
        return self.get_name()

    def connect(self, observer: str, data: str) -> bool:
        topics = dict(self.get_topic_names_and_types())
        data_topic = os.path.join(observer, 'data', data, 'value')
        if data_topic in topics:
            data_type = topics[data_topic][0]
            data_msg = msg_from_name(data_type)
            self.__client = DataClient(self, observer)
            self.__client.add_data(data, data_msg, data)
            return True
        return False

    def get(self, data: str) -> Tuple[Header, MsgType]:
        return self.__client.get_data(data)
