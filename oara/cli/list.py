from typing import Dict
from rclpy.node import Node
from rclpy.time import Time

from oara_interfaces.msg import ModuleInfo
from lifecycle_msgs.msg import State
from oara.internal.qos import SOFT_STATE_QOS
from oara.cli.cli import CLI, Live, Table


class List(Node, CLI):
    def __init__(self, node_name: str, namespace: str=None):
        Node.__init__(self, node_name=node_name, namespace=namespace)
        CLI.__init__(self)

        qos = SOFT_STATE_QOS
        qos.depth = 100
        self.__info_subscriber = self.create_subscription(ModuleInfo, 
            "oara/module_info", self.__info_callback, qos_profile=qos)

        self.__modules : Dict[str, ModuleInfo] = dict()
        self.__received_time : Dict[str, Time] = dict()
        self.header()
        self.__live = Live(self.__gen_table(), refresh_per_second=4)
        self.__live.start()
        self.__timer = self.create_timer(.25, self.__print)

    def __del__(self):
        self.__live.stop()
        
    def __info_callback(self, msg: ModuleInfo):
        self.get_logger().debug(f"received msg {msg}")
        self.__received_time[msg.header.frame_id] = self.get_clock().now()
        self.__modules[msg.header.frame_id] = msg

    def __print(self):
        self.__live.update(self.__gen_table())

    def __state_to_string(self, state: State) -> str:
        if state.id == State.PRIMARY_STATE_ACTIVE:
            return "[bold green]ACTIVE[/bold green]"
        elif state.id == State.PRIMARY_STATE_FINALIZED:
            return "[bold]FINALIZED[/bold]"
        elif state.id == State.PRIMARY_STATE_INACTIVE:
            return "[bold blue]INACTIVE[/bold blue]"
        elif state.id == State.PRIMARY_STATE_UNCONFIGURED:
            return "[bold yellow]UNCONFIGURED[/bold yellow]"
        elif state.id == State.TRANSITION_STATE_ACTIVATING:
            return "[bold italic]ACTIVATING[/bold italic]"
        elif state.id == State.TRANSITION_STATE_CLEANINGUP:
            return "[bold italic]CLEANINGUP[/bold italic]"
        elif state.id == State.TRANSITION_STATE_SHUTTINGDOWN:
            return "[bold italic]SHUTTINGDOWN[/bold italic]"
        elif state.id == State.TRANSITION_STATE_CONFIGURING:
            return "[bold italic]CONFIGURING[/bold italic]"
        elif state.id == State.TRANSITION_STATE_DEACTIVATING:
            return "[bold italic]DEACTIVATING[/bold italic]"
        elif state.id == State.TRANSITION_STATE_ERRORPROCESSING:
            return "[bold italic]ERRORPROCESSING[/bold italic]"
        else:
            return "[bold red]UNKNOWN[/bold red]"

    def __gen_table(self) -> Table:
        now = self.get_clock().now()

        table = Table(show_header=True, header_style="bold blue")
        table.add_column("Node")
        table.add_column("Module")
        table.add_column("Type")
        table.add_column("State", width=12, justify="right")
        table.add_column("Delay", justify="right")
        table.add_column("Last seen", justify="right")

        for m, i in self.__modules.items():
            r = self.__received_time[m]
            t = Time.from_msg(i.header.stamp)
            table.add_row(
                m,
                i.module,
                i.type.upper(),
                self.__state_to_string(i.state),
                f"{((r-t).nanoseconds / 1e9):10.04f}",
                f"{((now-r).nanoseconds / 1e9):10.04f}",
            )
        
        return table
