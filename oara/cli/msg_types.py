import importlib
from typing import Type
from ..internal import MsgType

def msg_from_name(msg_name: str) -> Type[MsgType]:
    # inspired from ros2cli/ros2service/ros2service/verb/call.py:requester
    try:
        parts = msg_name.split('/')
        if len(parts) == 2:
            parts = [parts[0], 'msg', parts[1]]

        module = importlib.import_module('.'.join(parts[:-1]))
        msg_name = parts[-1]
        msg_module = getattr(module, msg_name)
        return msg_module
    except (AttributeError, ModuleNotFoundError, ValueError):
        raise RuntimeError(f'The passed message type {msg_name} is invalid')

def request_msg_from_name(msg_name: str) -> Type[MsgType]:
    msg_module = msg_from_name(msg_name)
    try:
        msg = msg_module()
        msg.request.header
        msg.goal
        return msg_module
    except AttributeError:
        raise RuntimeError(f'The passed type {msg_name} is not an OARA Request message')
