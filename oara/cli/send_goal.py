import os
import yaml

from rclpy.node import Node
from rosidl_runtime_py import set_message_fields

from oara_interfaces.msg import GoalState

from oara.cli.msg_types import request_msg_from_name, msg_from_name
from oara.internal.actor_client import ActorClient
from oara.internal.task import Task

class SendGoal(Node):
    """Send a goal to an OARA actor."""
    def __init__(self, node_name: str):
        super().__init__(node_name)

    @property
    def name(self) -> str:
        return self.get_name()

    @property
    def state(self) -> GoalState:
        return self.__client.get_state(self.__task.id)

    def connect(self, actor: str) -> bool:
        topics = dict(self.get_topic_names_and_types())
        self.get_logger().debug(f"detected topics: { topics }")
        request_topic = os.path.join(actor, 'oara', 'request')
        if request_topic in topics:
            request_type = topics[request_topic][0]
            request_msg = request_msg_from_name(request_type)
            self.__client = ActorClient(self, actor, request_msg)
            return self.__client.wait_connection(timeout=3.0)
        return False

    def send(self, request: str, msg: str) -> bool:
        msg_type = msg_from_name(request)
        goal = msg_type()
        goal_values = yaml.safe_load(msg)
        if not isinstance(goal_values, dict):
            self.get_logger().error('The passed goal needs to be a dictionary in YAML format')
            return False
        set_message_fields(goal, goal_values)
        self.__task = Task(goal)
        return self.__client.traverse_lifecycle(self.__task)

    def retry_goal(self):
        """Dispatch again a goal reverted to an earlier state"""
        return self.__client.traverse_lifecycle(self.__task)

    def poll_state(self) -> GoalState:
        """Poll the state of a goal"""
        self.__client.process_event(self.__task.id)
        return self.state

    def result(self) -> GoalState:
        return self.__client.result(self.__task.id)

    def drop(self) -> bool:
        return self.__client.drop(self.__task.id)
