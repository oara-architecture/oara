from typing import Generic, TypeVar, NamedTuple
from oara_interfaces.msg import GoalRequest

SrvType = TypeVar("SrvType")
MsgType = TypeVar("MsgType")
class GoalRequestMsg_(NamedTuple):
    request: GoalRequest
    goal: MsgType

class GoalRequestMsg(GoalRequestMsg_, Generic[MsgType]):
    pass

import rclpy_lifecycle
CallbackReturn = rclpy_lifecycle.LifecycleNode.CallbackReturn

from .uuid import UID
from .module import Module