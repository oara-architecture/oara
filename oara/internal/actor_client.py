#!/usr/bin/env python3
from typing import Dict, Optional
from collections import defaultdict

import queue
import time

import rclpy
from rclpy.node import Node
from rclpy.time import Duration
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from rclpy.qos_event import SubscriptionEventCallbacks, QoSLivelinessChangedInfo

from oara_interfaces.msg import GoalState, GoalRequest

from .uuid import UID
from . import MsgType, GoalRequestMsg

from .task import Task
from .qos import HARD_STATE_QOS

class ActorClient:
    def __init__(self, node: Node, actor: str, msg: MsgType):
        self.__node = node
        self.__actor = actor
        self.__msg = msg

        self.__alive = False

        self.__goal_last_state : Dict[UID, GoalState] = dict()
        self.__goal_states : Dict[UID, GoalState] = dict()
        self.__goal_events : Dict[UID, queue.Queue[GoalState]] = defaultdict(queue.Queue)

        self.__cbgroup = MutuallyExclusiveCallbackGroup()
        self.__request_publisher = self.__node.create_publisher(msg, f'{actor}/oara/request',
            callback_group=self.__cbgroup,
            qos_profile=HARD_STATE_QOS)
        self.__response_subscriber = self.__node.create_subscription(GoalState, f'{actor}/oara/response', 
            self.__response_callback,
            callback_group=self.__cbgroup, 
            qos_profile=HARD_STATE_QOS,
            event_callbacks=SubscriptionEventCallbacks(liveliness=self.__liveliness))

    def destroy(self):
        if rclpy.ok():
            self.__node.destroy_subscription(self.__response_subscriber)
            self.__node.destroy_publisher(self.__request_publisher)

    def __liveliness(self, event: QoSLivelinessChangedInfo) -> None:
        self.__node.get_logger().debug(f"liveliness event received; alive count: {event.alive_count}")
        if event.alive_count_change > 0 and event.alive_count > 0:
            self.__node.get_logger().info(f"connected to actor client {self.__actor}")
            self.__alive = True
        elif event.alive_count_change < 0 and event.alive_count < 1:
            self.__node.get_logger().warn(f"disconnected from actor client {self.__actor}")
            self.__alive = False

    def get_state(self, goal_id: UID) -> GoalState:
        if goal_id in self.__goal_last_state:
            return self.__goal_last_state[goal_id]
        else:
            return GoalState(goal_id=goal_id, state=GoalState.UNKNOWN)

    def wait_connection(self, timeout: float) -> bool:
        # Sleep strategy as used in rclpy.client.Client.wait_for_service
        sleep_time = 0.1
        while (not self.__alive) and timeout > 0:
            time.sleep(sleep_time)
            timeout -= sleep_time
            # Hack because FastDDS does not support QosLiveliness
            info = self.__node.get_publishers_info_by_topic(self.__response_subscriber.topic_name)
            self.__alive = len(info) > 0
        
        return self.__alive

    def __response_callback(self, msg: GoalState):
        self.__node.get_logger().debug(f"Received feedback: {msg.goal_id} state {msg.state}")
        self.__goal_last_state[msg.goal_id] = msg
        self.__goal_events[msg.goal_id].put(msg)

    def __step(self, goal_id: UID, req: GoalRequestMsg, before: Optional[int] = None) -> bool:
        if not self.__alive:
            self.__node.get_logger().error(f"not connected to {self.__actor}")
            return False
        
        if before is not None:
            state = self.get_state(goal_id)
            if state.state != before:
                self.__node.get_logger().error(f"goal {goal_id} not in state {before}!")
                return False

        self.__node.get_logger().debug(f"Publishing request {req}")
        self.__request_publisher.publish(req)

        msg = self.__goal_events[goal_id].get()
        self.__goal_states[msg.goal_id] = msg
        self.__node.get_logger().debug(f"Received response {msg}")
        return True

    def formulate(self, goal_id: UID, goal: MsgType) -> bool:
        req : GoalRequestMsg = self.__msg()
        req.request.header.frame_id = self.__node.get_name()
        req.request.header.stamp = self.__node.get_clock().now().to_msg()
        req.request.goal_id = goal_id
        req.request.step = GoalRequest.FORMULATE
        req.goal = goal

        self.__node.get_logger().info(f"Calling 'formulate' with request {req}")
        if not self.__step(goal_id, req, GoalState.UNKNOWN):
            return False
        else:
            state = self.__goal_states[goal_id]
            return state.state == GoalState.FORMULATED

    def select(self, goal_id: UID) -> bool:
        req : GoalRequestMsg = self.__msg()
        req.request.header.frame_id = self.__node.get_name()
        req.request.header.stamp = self.__node.get_clock().now().to_msg()
        req.request.goal_id = goal_id
        req.request.step = GoalRequest.SELECT

        self.__node.get_logger().info(f"Calling 'select' with request {req}")
        if not self.__step(goal_id, req, GoalState.FORMULATED):
            return False
        else:
            state = self.__goal_states[goal_id]
            return state.state == GoalState.SELECTED

    def expand(self, goal_id: UID) -> bool:
        req : GoalRequestMsg = self.__msg()
        req.request.header.frame_id = self.__node.get_name()
        req.request.header.stamp = self.__node.get_clock().now().to_msg()
        req.request.goal_id = goal_id
        req.request.step = GoalRequest.EXPAND

        self.__node.get_logger().info(f"Calling 'expand' with request {req}")
        if not self.__step(goal_id, req, GoalState.SELECTED):
            return False
        else:
            state = self.__goal_states[goal_id]
            return state.state == GoalState.EXPANDED

    def commit(self, goal_id: UID, plan_id: UID) -> bool:
        req : GoalRequestMsg = self.__msg()
        req.request.header.frame_id = self.__node.get_name()
        req.request.header.stamp = self.__node.get_clock().now().to_msg()
        req.request.goal_id = goal_id
        req.request.plan_id = plan_id
        req.request.step = GoalRequest.COMMIT

        self.__node.get_logger().info(f"Calling 'commit' with request {req}")
        if not self.__step(goal_id, req, GoalState.EXPANDED):
            return False
        else:
            state = self.__goal_states[goal_id]
            return state.state == GoalState.COMMITTED

    def dispatch(self, goal_id: UID) -> bool:
        req : GoalRequestMsg = self.__msg()
        req.request.header.frame_id = self.__node.get_name()
        req.request.header.stamp = self.__node.get_clock().now().to_msg()
        req.request.goal_id = goal_id
        req.request.step = GoalRequest.DISPATCH

        self.__node.get_logger().info(f"Calling 'dispatch' with request {req}")
        if not self.__step(goal_id, req, GoalState.COMMITTED):
            return False
        else:
            state = self.__goal_states[goal_id]
            return state.state == GoalState.DISPATCHED

    def drop(self, goal_id: UID) -> bool:
        if self.__goal_states[goal_id].state == GoalState.DROPPED:
            self.__node.get_logger().debug(f"Goal {goal_id} already dropped")
            return True

        req : GoalRequestMsg = self.__msg()
        req.request.header.frame_id = self.__node.get_name()
        req.request.header.stamp = self.__node.get_clock().now().to_msg()
        req.request.goal_id = goal_id
        req.request.step = GoalRequest.DROP

        self.__node.get_logger().info(f"Calling 'drop' with request {req}")
        if not self.__step(goal_id, req):
            return False
        else:
            state = self.__goal_states[goal_id]
            return state.state == GoalState.DROPPED

    def result(self, goal_id: UID) -> GoalState:
        self.__node.get_logger().debug(f"Waiting for result of {goal_id}")
        state : int = self.get_state(goal_id).state
        if state != GoalState.DISPATCHED:
            self.__node.get_logger().error(f"goal {goal_id} not in state DISPATCHED!")
            return self.get_state(goal_id)

        while state not in [GoalState.FINISHED, GoalState.DROPPED, GoalState.FORMULATED]:
            msg = self.__goal_events[goal_id].get()
            self.__node.get_logger().debug(f"Received status {msg}")
            self.__goal_states[goal_id] = msg
            state = msg.state

        return self.get_state(goal_id)

    def process_event(self, goal_id: UID) -> bool:
        self.__node.get_logger().debug(f"Polling status of {goal_id}")
        # state = self.get_state(goal_id)
        # if state.state != GoalState.DISPATCHED:
        #     self.__node.get_logger().error(f"goal {goal_id} not in state DISPATCHED!")
        #     return False
        try:
            msg = self.__goal_events[goal_id].get_nowait()
            self.__node.get_logger().debug(f"processing event {msg}")
            self.__goal_states[goal_id] = msg
            return True
        except queue.Empty:
            return False

    def traverse_lifecycle(self, task: Task, to: int = GoalState.DISPATCHED) -> bool:
        """ Traverse de lifecycle of a task up to a target state.

        :param task: task goal.
        :param to: the target state.
        :return: True if task is dispatched.
        """

        task.state = self.get_state(task.id).state

        logger = self.__node.get_logger()

        if task.state == GoalState.UNKNOWN and task.state < to:
            logger.info(f"[actor client {self.__actor}] formulating task {task.id}")
            transition_result = self.formulate(task.id, task.goal)
            task.state = self.get_state(task.id).state
            if transition_result:
                logger.info(f"[actor client {self.__actor}] FORMULATED {task.id}")
            else:
                logger.error(f"[actor client {self.__actor}] FORMULATE {task.id} failed")
                return False

        if task.state == GoalState.FORMULATED and task.state < to:
            logger.info(f"[actor client {self.__actor}] selecting task {task.id}")
            transition_result = self.select(task.id)
            task.state = self.get_state(task.id).state
            if transition_result:
                logger.info(f"[actor client {self.__actor}] SELECTED {task.id}")
            else:
                logger.error(f"[actor client {self.__actor}] SELECT {task.id} failed")
                return False

        if task.state == GoalState.SELECTED and task.state < to:
            logger.info(f"[actor client {self.__actor}] expanding task {task.id}")
            transition_result = self.expand(task.id)
            task.state = self.get_state(task.id).state
            if transition_result:
                logger.info(f"[actor client {self.__actor}] EXPANDED {task.id}")
            else:
                logger.error(f"[actor client {self.__actor}] EXPAND {task.id} failed")
                return False

        if task.state == GoalState.EXPANDED and task.state < to:
            logger.info(f"[actor client {self.__actor}] committing task {task.id}")
            plan_id = self.get_state(task.id).expansions[0].plan_id
            transition_result = self.commit(task.id, plan_id)
            task.state = self.get_state(task.id).state
            if transition_result:
                logger.info(f"[actor client {self.__actor}] COMMITTED {task.id}")
            else:
                logger.error(f"[actor client {self.__actor}] COMMITTED {task.id} failed")
                return False

        if task.state == GoalState.COMMITTED and task.state < to:
            logger.info(f"[actor client {self.__actor}] dispatching task {task.id}")
            transition_result = self.dispatch(task.id)
            task.state = self.get_state(task.id).state
            if transition_result:
                logger.info(f"[actor client {self.__actor}] DISPATCHED {task.id}")
                return True
            else:
                logger.error(f"[actor client {self.__actor}] DISPATCH {task.id} failed")
                return False

        task.state = self.get_state(task.id).state
        return False
