#!/usr/bin/env python3
from functools import partial
from logging.config import valid_ident
from multiprocessing import Event
import time
from typing import Dict, Tuple
import threading

from pyrsistent import v

import rclpy
from rclpy.node import Node, Subscription
from rclpy.time import Time, Duration
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup, ReentrantCallbackGroup
from rclpy.qos_event import SubscriptionEventCallbacks, QoSLivelinessChangedInfo

from std_msgs.msg import String, Header
from oara_interfaces.msg import DataTrigger

from .qos import EVENT_QOS
from .. import MsgType


class DataClient:
    def __init__(self, node: Node, observer: str):
        self.__node = node
        self.__observer = observer

        self.__data : Dict[str, MsgType] = dict()
        self.__headers : Dict[str, Header] = dict()
        self.__value_subscribers : Dict[str, Subscription] = dict()
        self.__header_subscribers : Dict[str, Subscription] = dict()
        self.__events : Dict[str, Tuple[threading.Event, threading.Event]] = dict()
        self.__alive : Dict[str, bool] = dict()

        self.__cbgroup = ReentrantCallbackGroup()
        self.__trig_publisher = self.__node.create_publisher(DataTrigger, f"{observer}/data/trigger",
            qos_profile=EVENT_QOS)

    def add_data(self, data: str, msg_type: MsgType, remote_data: str) -> bool:
        if data in self.__alive:
            self.__node.get_logger().error(f"data {data} subscriber already registered to observer {self.__observer}")
            return False

        self.__value_subscribers[data] = self.__node.create_subscription(msg_type, f'{self.__observer}/data/{remote_data}/value', 
            partial(self.__data_callback, data),
            callback_group=self.__cbgroup, 
            qos_profile=EVENT_QOS)

        self.__header_subscribers[data] = self.__node.create_subscription(Header, f'{self.__observer}/data/{remote_data}/header', 
            partial(self.__header_callback, data),
            callback_group=self.__cbgroup,
            qos_profile=EVENT_QOS,
            event_callbacks=SubscriptionEventCallbacks(liveliness=partial(self.__liveliness_changed, data)))

        self.__alive[data] = False
        self.__events[data] = (threading.Event(), threading.Event())

    def destroy(self):
        if rclpy.ok():
            for s in self.__value_subscribers.values(): self.__node.destroy_subscription(s)
            for s in self.__header_subscribers.values(): self.__node.destroy_subscription(s)
            self.__node.destroy_publisher(self.__trig_publisher)

    def __liveliness_changed(self, data: str, event: QoSLivelinessChangedInfo) -> None:
        self.__node.get_logger().debug(f"liveliness received; alive count: {event.alive_count}")
        if event.alive_count_change > 0 and event.alive_count > 0:
            self.__node.get_logger().info(f"connected to client {self.__observer}")
            self.__alive[data] = True
        elif event.alive_count_change < 0 and event.alive_count < 1:
            self.__node.get_logger().warn(f"disconnected from client {self.__observer}")
            self.__alive[data] = False

    def get_data(self, data: str) -> Tuple[Header, MsgType]:
        if data in self.__events:
            h, v = self.__events[data]
            h.clear()
            v.clear()
            trig = DataTrigger(data=data)
            trig.header.frame_id = self.__node.get_name()
            trig.header.stamp = self.__node.get_clock().now().to_msg()
            self.__trig_publisher.publish(trig)

            self.__node.get_logger().debug(f"waiting value event")
            #while not (h.is_set() and v.is_set()):
            #    time.sleep(0.1)
            h.wait()
            v.wait()
            self.__node.get_logger().debug(f"data received")
            return self.__headers[data], self.__data[data]

        else:
            self.__node.get_logger().error(f"data {data} unknown")
            return None, None

    def wait_connection(self, timeout: float) -> bool:
        # Sleep strategy as used in rclpy.client.Client.wait_for_service
        sleep_time = 0.1
        while (not all(self.__alive.values())) and timeout > 0.0:
            time.sleep(sleep_time)
            timeout -= sleep_time
            # Hack because FastDDS does not support QosLiveliness
            for k, v in self.__alive.items():
                info = self.__node.get_publishers_info_by_topic(self.__header_subscribers[k].topic_name)
                self.__alive[k] = len(info) > 0
        return all(self.__alive.values())

    def __data_callback(self, data: str, msg: MsgType):
        self.__node.get_logger().debug(f"Received data {data} value {msg}")
        self.__data[data] = msg
        self.__events[data][0].set()
        self.__node.get_logger().debug(f"event set")

    def __header_callback(self, data: str, msg: Header):
        self.__node.get_logger().debug(f"Received data {data} header {msg}")
        self.__headers[data] = msg
        self.__events[data][1].set()
        self.__node.get_logger().debug(f"event set")
