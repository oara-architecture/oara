import enum

class DocEnum(enum.Enum):
    def __new__(cls, value, doc: str = None):
        self = object.__new__(cls)  # calling super().__new__(value) here would fail
        self._value_ = value
        if doc is not None:
            self.__doc__ = doc
        return self