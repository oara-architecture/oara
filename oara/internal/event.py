from .docenum import DocEnum, enum


class EventReaction(DocEnum):
    """Enumeration of reaction strategies to incoming events."""

    REPLAN = enum.auto(), "Replan all the goals that are below SELECTED"
    REPLAN_DISPATCHED_GOALS = enum.auto(), "Replan all the goals that are currently DISPATCHED"
    DEFER = enum.auto(), "Defer all the goals that are below FORMULATED"
    DEFER_DISPATCHED_GOALS = enum.auto(), "Defer all the goals that are currently DISPATCHED"
    REFORM = enum.auto(), "Reform all the goals that are below FORMULATED"
    REFORM_DISPATCHED_GOALS = enum.auto(), "Reformm all the goals that are currently DISPATCHED"
    REPAIR = enum.auto(), "Repair all the goals that are below EXPANDED"
    REPAIR_DISPATCHED_GOALS = enum.auto(), "Repair all the goals that are currently DISPATCHED"
    CONTINUE = enum.auto(), "Continue all the goals that are below COMMITTED"
    CONTINUE_DISPATCHED_GOALS = enum.auto(), "Continue all the goals that are currently DISPATCHED"
    PROCESS = enum.auto(), "Call the PROCESS transition"