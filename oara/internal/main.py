from typing import Type
import rclpy
import rclpy.logging
import rclpy.executors

from .module import Module
from ..cli.cli import CLI

def main(module: Type[Module], *vargs, num_threads=4, **kwargs):
    """ Helper function to initialize an OARA Module node.

    This function will initialize ROS2 if needed, then create the module object,
    and call its `create` function to initialize it. It will then spin
    the node using a ROS2 MultiThreaded Executor.

    :param module: Module class
    :param vargs: Module arguments
    :param loglevel: ROS2 Logging level
    :param num_threads: Number of threads in the MultiThreadedExecutor
    :param kwargs: Module named arguments
    """
    cli = CLI()

    if not rclpy.ok():
        rclpy.init()
    node = module(*vargs, **kwargs)
    node.get_logger().debug("Node created")
    executor = rclpy.executors.MultiThreadedExecutor(num_threads=num_threads)
    executor.add_node(node)
    
    node.get_logger().debug("Start spinning")
    
    try:
        executor.spin()
    
    except KeyboardInterrupt:
        cli.console.print()

    except Exception as ex:
        node.get_logger().error(f"{ex}")
        cli.console.print_exception(show_locals=True)

    finally:
        node.get_logger().debug("Stop spinning")
        if rclpy.ok():
            rclpy.shutdown()
            #executor_thread.join()
