from abc import ABC, abstractmethod
from typing import Any, Optional, final, Dict, Callable
import os

import rclpy
import rclpy.node
import rclpy.task
from rclpy.parameter import Parameter, ParameterType
from rcl_interfaces.msg import ParameterDescriptor
from lifecycle_msgs.msg import State
from rclpy_lifecycle import LifecycleNode, State
from rclpy.callback_groups import MutuallyExclusiveCallbackGroup, ReentrantCallbackGroup

from oara_interfaces.msg import ModuleInfo
from .qos import SOFT_STATE_QOS
from ..internal import MsgType
from ..cli.cli import CLI


class Module(LifecycleNode, CLI, ABC):
    """ An OARA ROS2 Module. 
    
    A module is a LifecycleNode.
    The user can redefine hooks for the several steps of the lifecycle.
    http://design.ros2.org/articles/node_lifecycle.html

    The requirements to follow in defining these hooks in OARA is:
    
    - in the constructor of your module, declare the parameters, data, events...
    - in the configuration/cleanup stages, check the values of these parameters to manage your own structure, read files, ...
    - in the activation/deactivation states, manage connections with external processes, initialize streams, ...

    These hooks must return a CallbackReturn value.
    """

    def __init__(self, name: str):
        """
        :param name: module name.
        """
        LifecycleNode.__init__(self, name, allow_undeclared_parameters=True)
        CLI.__init__(self)
        self._model = ModuleInfo()
        self._model.module = name
        self._model.header.frame_id = os.path.join(self.get_namespace(), self.get_name())
        self._model.period = 1.0
        
        self.add_parameter("oara.period", Parameter.Type.DOUBLE, default_value=1.0,
            description="Period of the main loop")
        self.__info_publisher = self.create_publisher(ModuleInfo, '/oara/module_info',
            qos_profile=SOFT_STATE_QOS)
        self.__info_timer = self.create_timer(1.0, self.__info_loop)

    @property
    def state(self) -> int:
        """ Module Node lifecycle state. """
        return self.get_current_state().id

    @property
    def state_label(self) -> str:
        """ Module Node lifecycle state label. """
        return self.get_current_state().label

    @property
    def model(self) -> ModuleInfo:
        """Module OARA model. """
        return self._model

    @property
    def configured(self) -> bool:
        """ Configuration status of the module. """
        return self.state == State.PRIMARY_STATE_INACTIVE or self.state == State.PRIMARY_STATE_ACTIVE

    @property
    def active(self) -> bool:
        """ Activation status of the module. """
        return self.state == State.PRIMARY_STATE_ACTIVE

    @property
    def name(self) -> str:
        """ Module name. """
        return self.get_name()

    def on_configure(self, state: State) -> LifecycleNode.CallbackReturn:
        cb_success = super().on_configure(state)
        if cb_success == LifecycleNode.CallbackReturn.SUCCESS:
            self.model.period = self.get_parameter('oara.period').value
            self.get_logger().debug(f"-- period: {self.model.period}")
            self.__loop_timer = self.create_timer(self.model.period, self.loop, 
                callback_group=MutuallyExclusiveCallbackGroup())

        return cb_success

    def on_cleanup(self, previous_state: State) -> LifecycleNode.CallbackReturn:
        self.__loop_timer.cancel()
        self.destroy_timer(self.__loop_timer)
        return super().on_cleanup(previous_state)

    def loop(self):
        self.get_logger().debug(f"Main loop")
        self._loop()
        self.get_logger().debug(f".. loop end")
        #self.__loop_timer.reset()

    @abstractmethod
    def _loop(self):
        pass

    def __info_loop(self):
        self.model.header.stamp = self.get_clock().now().to_msg()
        self.model.state = State(id=self.state, label=self.state_label)
        self.__info_publisher.publish(self.model)

    @final
    def create_task(self, function: Callable, *args, **kwargs) -> rclpy.task.Task:
        """ Create and schedule a new task in the ROS executor. 
        
        :param function: function to execute.
        :param args: function arguments.
        :param kwargs: function named arguments.
        """
        return self.executor.create_task(function, *args, **kwargs)

    @final
    def add_parameter(self, name: str, type: Parameter.Type, 
                      description: str, default_value: Any = None):
        """ Add a new ROS parameter to the node.

        :param name: parameter name.
        :param type: parameter type; must be one of the rclpy.parameter.Parameter.Type values
        :param description: parameter description.
        :param default_value: default value of the parameter.
        """
        if type == Parameter.Type.BOOL:
            param_type = ParameterType.PARAMETER_BOOL
        elif type == Parameter.Type.BOOL_ARRAY:
            param_type = ParameterType.PARAMETER_BOOL_ARRAY
        elif type == Parameter.Type.BYTE_ARRAY:
            param_type = ParameterType.PARAMETER_BYTE_ARRAY
        elif type == Parameter.Type.DOUBLE:
            param_type = ParameterType.PARAMETER_DOUBLE
        elif type == Parameter.Type.DOUBLE_ARRAY:
            param_type = ParameterType.PARAMETER_DOUBLE_ARRAY
        elif type == Parameter.Type.INTEGER:
            param_type = ParameterType.PARAMETER_INTEGER
        elif type == Parameter.Type.INTEGER_ARRAY:
            param_type = ParameterType.PARAMETER_INTEGER_ARRAY
        elif type == Parameter.Type.STRING:
            param_type = ParameterType.PARAMETER_STRING
        elif type == Parameter.Type.STRING_ARRAY:
            param_type = ParameterType.PARAMETER_STRING_ARRAY
        else:
            param_type = ParameterType.PARAMETER_NOT_SET

        descriptor = ParameterDescriptor(type=param_type, 
                                         description=description)
        self.declare_parameter(name, value=default_value,
                               descriptor=descriptor)

    @final
    def is_parameter_set(self, name: str) -> bool:
        p = self.get_parameter_or(name, None)
        self.get_logger().debug(f"parameter {name} type: {p.type_}")
        return (p.type_ != Parameter.Type.NOT_SET)

    def on_error(self, state: State) -> LifecycleNode.CallbackReturn:
        self.get_logger().warn(f"Error in transition from state {state}. Returning to state 'unconfigured'")
        return LifecycleNode.CallbackReturn.SUCCESS
