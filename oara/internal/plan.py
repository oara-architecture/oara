from typing import Iterable, List, Tuple, Union
import json
import networkx
from networkx.readwrite import json_graph
import rclpy.time
import rclpy.clock

import oara_interfaces.msg
from ..internal.uuid import UID
from ..internal import MsgType
from .task import Task


class TaskJsonEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Task):
            return {'task': str(o)}
        return super().default(o)


class Plan:
    """Plan structure."""

    def __init__(self):
        self.__graph = networkx.DiGraph()
        self.__reverse = networkx.DiGraph()

    def add_task(self, task_id: int, goal: MsgType, actor: str, dispatch_time: rclpy.time.Time=rclpy.time.Time(seconds=0, nanoseconds=0, clock_type=rclpy.clock.ClockType.ROS_TIME)):
        """ Add a new task to the plan.

        :param task_id: ID of the task
        :param goal: Task goal as a ROS2 message
        :param actor: Actor to which the task will be sent
        """
        self.__graph.add_node(task_id, task=Task(goal, actor, dispatch_time), label=f"[{task_id}:{actor}] {goal} (@{dispatch_time})")

    def get_tasks(self) -> Iterable[Tuple[int, Task]]:
        return self.__graph.nodes(data="task")

    def get_task(self, task_id: int) -> Task:
        """Get task object associated to index.

        :param task_id: task index in the plan.
        :return: the Task object
        """
        return self.__graph.nodes[task_id]["task"]

    def remove_task(self, task_id: int):
        """ Remove a task from the plan.

        :param task_id: ID of the task to remove.
        """
        self.__graph.remove_node(task_id)


    def add_precedence(self, before: int, after: int):
        """ Add a precedence relation between two tasks.

        `before` < `after`

        :param before: ID of the task before `after`
        :param after: ID of the task after `before`
        """
        if not self.__graph.has_node(before):
            raise networkx.NodeNotFound(before)
        if not self.__graph.has_node(after):
            raise networkx.NodeNotFound(after)
        self.__graph.add_edge(before, after)
    
    def __len__(self) -> int:
        return self.__graph.number_of_nodes()

    def add_sequence(self, tasks: List[Union[Tuple[int, MsgType, str], Tuple[int, MsgType, str, rclpy.time.Time]]]):
        """ Add a sequence of tasks to the plan.

        :param tasks: a list of task, each element is a tuple (id, goal, actor)
        """
        for t in tasks:
            self.add_task(*t)
        
        for (a, b) in zip(tasks[:-1], tasks[1:]):
            self.add_precedence(a[0], b[0])

    def write(self, file):
        """ Write plan to file.

        :param file: output file
        """
        networkx.drawing.nx_agraph.write_dot(self.__graph, file)

    def to_json(self) -> str:
        """ Export the plan as a JSON Graph

        :return: a JSON string representing nodes and edges of the plan.
        """
        return json.dumps(json_graph.node_link_data(self.__graph), cls=TaskJsonEncoder)

    def to_dot(self) -> str:
        """ Export the plan as a Dot string

        :return: a string representing the graph in DOT
        """
        G = networkx.drawing.nx_agraph.to_agraph(self.__graph)
        return G.to_string()

    def to_msg(self) -> oara_interfaces.msg.Plan:
        """Convert this plan into a ROS2 Plan message."""
        msg = oara_interfaces.msg.Plan()
        for i in self.__graph:
            task : Task = self.__graph.nodes[i]["task"]
            msg.tasks.append(task.to_msg(i))
        for i, nbrs in self.__graph.adjacency():
            for j in nbrs:
                msg.ordering.append(oara_interfaces.msg.TaskOrder(before=i, after=j))
        return msg

    def freeze(self):
        self.__reverse = networkx.reverse_view(self.__graph)
        networkx.freeze(self.__graph)

    def predecessors(self, node: int) -> Iterable[int]:
        return self.__reverse[node]

    def successors(self, node: int) -> Iterable[int]:
        return self.__graph[node]

    def is_terminated(self) -> bool:
        for n in self.__graph:
            t : Task = self.__graph.nodes[n]["task"]
            if t.state != oara_interfaces.msg.GoalState.FINISHED:
                return False
        return True
