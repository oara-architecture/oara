from queue import PriorityQueue


class PrioritySet(PriorityQueue):
    def _put(self, item):
        if item not in self.queue:
            super(PrioritySet, self)._put(item)