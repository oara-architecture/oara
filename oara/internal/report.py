from oara_interfaces.msg import GoalState


class DispatchReport:
    """ A class to represent monitored reports from children actors

    This class is used to send results from monitoring to evaluation.
    """

    def __init__(self, actor: str, dispatch_status: bool, 
                 state: int = GoalState.UNKNOWN, ):
        """
        :param actor: actor that reports this event
        :param dispatch_status: whether the dispatch step was accepted or not
        :param state: the final state of execution
        """
        self.__result = dispatch_status
        self.__actor = actor
        self.__state = state

    @property
    def status(self) -> bool:
        """Dispatch status."""
        return self.__result

    @property
    def actor(self) -> str:
        """Actor name."""
        return self.__actor

    @property
    def state(self) -> int:
        """Final subgoal lifecycle state."""
        return self.__state

    def __str__(self) -> str:
        return f"{self.state} ({self.status}) [{self.actor}]"
