from typing import Iterable, Optional

from oara_interfaces.msg import GoalState
from oara_interfaces.msg import Task as TaskMsg

from .uuid import UID
from ..internal import MsgType
import rclpy.time
import rclpy.clock


class Task:
    """ A class to represent subgoal Task, managed by dispatchers.
    """

    def __init__(self, goal: MsgType, actor: Optional[str] = None,
                 dispatch_time: rclpy.time.Time = rclpy.time.Time(
                     seconds=0, nanoseconds=0,
                     clock_type=rclpy.clock.ClockType.SYSTEM_TIME),
                state: int = GoalState.UNKNOWN):
        """
        :param goal: task goal
        :param actor: actor that will receive the task
        """
        self.__id = UID()
        self.__goal = goal
        self.__actor = actor
        self.__state = state
        self.__dispatch_time = dispatch_time

    @property
    def id(self) -> UID:
        return self.__id

    @property
    def goal(self) -> MsgType:
        return self.__goal

    @property
    def actor(self) -> Optional[str]:
        return self.__actor

    @property
    def state(self) -> int:
        return self.__state

    @state.setter
    def state(self, value: int):
        self.__state = value

    @property
    def dispatch_time(self) -> rclpy.time.Time:
        return self.__dispatch_time

    def __iter__(self) -> Iterable:
        return (self.goal, self.actor, self.state, self.__dispatch_time).__iter__()

    def __str__(self) -> str:
        return f"{self.goal} [{self.actor}] ({self.dispatch_time.nanoseconds/ rclpy.time.CONVERSION_CONSTANT})"

    def to_msg(self, index: int = -1) -> TaskMsg:
        """Convert this task into a ROS2 Task message."""
        msg = TaskMsg()
        msg.index = index
        msg.goal = str(self.__goal)
        msg.actor = str(self.__actor)
        msg.state = self.__state
        msg.dispatch_time = self.__dispatch_time.to_msg()
        return msg