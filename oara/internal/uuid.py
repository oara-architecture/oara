import uuid

class UID(str):
    """An OARA Unique ID as a string. 
    
    Used to identify goals and clients in topics.
    """
    
    def __new__(cls):
        uid = uuid.uuid4()
        return str.__new__(cls, str(uid))
