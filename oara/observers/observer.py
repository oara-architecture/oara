from typing import Callable, Tuple, final, Optional, Dict
from abc import ABC

import rclpy.service
import reelay

from rclpy.publisher import Publisher
from rclpy.callback_groups import ReentrantCallbackGroup
from std_msgs.msg import Header
import rclpy_lifecycle
import collections
import functools

from std_msgs.msg import Header, String
from oara_interfaces.msg import Event, DataInfo, ModuleInfo, DataTrigger, EventInfo

from ..internal.module import Module
from ..internal import MsgType, CallbackReturn, SrvType
from ..internal.qos import EVENT_QOS
from ..internal.priority_set import PrioritySet


class Observer(Module, ABC):
    """ A ROS2 OARA Observer.

    It is implemented as a ROS2 node that provides servers to access data.
    """

    def __init__(self, name: str):
        """
        :param name: observer name.
        """
        super().__init__(name)
        self.__cbgroup = ReentrantCallbackGroup()
        self.__fun_cbgroup = ReentrantCallbackGroup()
        # Data management structures
        self.__data_types : Dict[str, MsgType] = dict()
        self.__data_value : Dict[str, MsgType] = dict()
        self.__data_header : Dict[str, Header] = dict()
        self.__data_publishers : Dict[str, Tuple[Publisher, Publisher]] = dict()
        self.__requested_data : PrioritySet[str] = PrioritySet()
        self.__data_subscriber = self.create_subscription(DataTrigger, "~/data/trigger", self._data_trigger,
            qos_profile=EVENT_QOS, callback_group=self.__cbgroup)
        self.__functions = collections.defaultdict(dict)
        self.__data_fun_types : Dict[str, SrvType] = dict()
        self.__servers: Dict[str, rclpy.service.Service] = dict()
        #
        self.__monitors : Dict[str, Dict[str, reelay.discrete_timed_monitor]] = collections.defaultdict(dict)
        self.__monitors_formulas : Dict[str, str] = dict()
        self.__monitors_observations = dict()
        self.__event_publishers : Dict[str, Dict[str, Publisher]] = collections.defaultdict(dict)
        # self.__event_qos = QoSProfile(durability=DurabilityPolicy.TRANSIENT_LOCAL,
        #                               history=HistoryPolicy.KEEP_ALL)
        
        self.model.type = ModuleInfo.OBSERVER

    def on_configure(self, state: rclpy_lifecycle.State) -> CallbackReturn:
        cb_success = super().on_configure(state)
        if cb_success == CallbackReturn.SUCCESS:
            for data in self.__data_types:
                msg = self.__data_types[data]
                self.get_logger().debug(f"create data {data}")
                value_pub = self.create_publisher(msg, f"~/data/{data}/value", qos_profile=EVENT_QOS)
                header_pub = self.create_publisher(Header, f"~/data/{data}/header", qos_profile=EVENT_QOS)
                self.__data_publishers[data] = header_pub, value_pub

                for func in self.__functions[data]:
                    self.get_logger().debug(f"create data function service '{data}/{func}'")
                    srv = self.__data_fun_types[f"{data}/{func}"]
                    self.__servers[f"{data}/{func}"] = self.create_service(srv, f"~/{data}/{func}",
                                                                    functools.partial(self._data_fun, data, func),
                                                                    callback_group=self.__fun_cbgroup)

            for data, monitor in self.__monitors.items():
                for name in monitor.keys():
                    self.get_logger().debug(f"create event topic '{name}'")
                    self.__event_publishers[data][name] = self.create_publisher(Event, f"~/event/{name}", qos_profile=EVENT_QOS)
        return cb_success

    def on_cleanup(self, state: rclpy_lifecycle.State) -> CallbackReturn:
        for (h, v) in self.__data_publishers.values():
            self.destroy_publisher(h)
            self.destroy_publisher(v)
        for srv in self.__servers.values():
            srv.destroy()
        for data, publishers in self.__event_publishers.items():
           for event, publisher in publishers.items():
               self.get_logger().debug(f"destroy publisher {data}.{event}")
               self.destroy_publisher(publisher)
        return super().on_cleanup(state)

    ###------------------------ LOOP ---###
    def _loop(self):
        super()._loop()
        while not self.__requested_data.empty():
            data = self.__requested_data.get_nowait()
            self.get_logger().info(f"publishing data {data}")
            p, q = self.__data_publishers[data]
            p.publish(self.__data_header[data])
            q.publish(self.__data_value[data])

    ###------------------------ DATA ---###
    def add_data(self, name: str, msg: MsgType, initial: Optional[MsgType] = None) -> bool:
        """ Declare a new data in this module.

        :param name: data name.
        :param msg: ROS2 message type of the data.
        :return: True if the data has been added.
        """
        if name in self.__data_types:
            self.get_logger().warn(f"data {name} already added")
            return False

        if self.active:
            self.get_logger().warn(f"cannot add new data when the module is active")
            return False

        self.__data_types[name] = msg
        if initial is not None: 
            self.set_data(name, initial)
        self.get_logger().debug(f"added data {name}")
        self.model.data.append(DataInfo(name=name, type=msg.__name__))
        return True

    def _data_trigger(self, msg: DataTrigger):
        if not self.active:
            self.get_logger().warning(f"{self.name} not active: cannot provide data {msg.data}")
            return
        self.__requested_data.put(msg.data)

    @final
    def set_data(self, name: str, value: MsgType):
        """ Set data value.

        :param name: data name.
        :param value: data value.
        """
        self.__data_value[name] = value
        self.__data_header[name] = Header(stamp = self.get_clock().now().to_msg(), frame_id=self.name)
        self.__update_monitors(name)

    @final
    def get_data(self, name: str) -> MsgType:
        """ Get a data stored in this observer.

        :param name: data name.
        :return: The data value.
        """
        return self.__data_value[name]

    def _data_fun(self, data: str, function: str, request: 'SrvType.Request', response: 'SrvType.Response'):
        if not self.active:
            self.get_logger().warning(f"{self.name} not active: cannot call function {data}.{function}")
            return response
        self.get_logger().debug(f"data {data} -- calling function {function} with args {request}")
        try:
            data_value = self.__data_types[data]()
            if data in self.__data_value:
                data_value = self.__data_value[data]

            response = self.__functions[data][function](data_value, request)
        except Exception as e:
            self.get_logger().error(f"{data}/{function} ({request}) callback failed with: {e.__class__.__qualname__} {str(e)}")
        return response

    @final
    def add_function(self, data: str, name: str, srv: SrvType, function: Callable) -> bool:
        """Add a new function bound to a data.
        
        :param data: data name.
        :param name: function name.
        :param srv: function service type.
        :param function: actual function.
        :return: True if the function has been added.
        """
        if self.configured:
            self.get_logger().warn(f"cannot add new function when the module is configured")
            return False
        if not data in self.__data_types:
            self.get_logger().error(f"data {data} not registered! cannot add function {name}")
            return False
        if name in self.__functions[data]:
            self.get_logger().error(f"function {name} already registered for data {data}")
            return False
        self.__functions[data][name] = function
        self.get_logger().info(f"create data function '{data}/{name}'")
        self.__data_fun_types[f"{data}/{name}"] = srv
        return True


    ###------------------------ EVENTS MONITORING ---###
    @final
    def add_data_monitor(self, name: str, data: str, monitor: str,
                        observations: Dict[str, Callable] = dict()) -> bool:
        """Add a new monitor bound to a data.
        
        :param data: data name.
        :param name: monitor name.
        :param monitor: monitor formula.
        :return: True if the function has been added.
        """
        if name in self.__monitors[data]:
            self.get_logger().error(f"Monitor {name} already registerd for data {data}")
            return False
        if self.configured:
            self.get_logger().warn(f"cannot add new monitor when the module is configured")
            return False
        
        self.__monitors[data][name] = reelay.discrete_timed_monitor(pattern=monitor, condense=False)
        self.__monitors_formulas[name] = monitor
        self.__monitors_observations[name] = observations
        self.get_logger().debug(f"Monitor [{name}] {monitor} added")
        self.model.events.append(EventInfo(name=name, data=data, monitor=monitor))
        return True

    def __update_monitors(self, data: str):
        for name, monitor in self.__monitors[data].items():
            time = self.__data_header[data].stamp
            value = self.__data_value[data]
            obs = {o: f(time, value) for o, f in self.__monitors_observations[name].items()}
            if hasattr(value, 'data'):
                obs.update({f'{data}': value.data})
            self.get_logger().debug(f"updating monitor {name}: {obs}")
            result = monitor.update(obs)
            if result['value'] and self.active:
                event = Event(header=Header(stamp=time, frame_id=self.name),
                              value=True,
                              monitor=name,
                              data=data)
                self.get_logger().debug(f"monitor {name} True: publishing {event}")
                self.__event_publishers[data][name].publish(event)
                