from typing import Callable, Dict, NoReturn

from oara_interfaces.msg import ModuleInfo, DataTrigger
from .observer import Observer


class OnGetCallbackObserver(Observer):
    """ A ROS2 OARA Observer that has callbacks associated to getters.
    """

    def __init__(self, name: str):
        super().__init__(name)
        self.model.pattern = ModuleInfo.OBSERVER_ONGET
        self.__callbacks : Dict[str, Callable[[DataTrigger], NoReturn]] = dict()

    def register_onget_callback(self, data: str, callback: Callable[[DataTrigger], NoReturn]):
        """Register a callback called when an actor calls the data getter.

        :param data: data name.
        :param callback: callback function.
        """
        self.__callbacks[data] = callback

    def _data_trigger(self, msg: DataTrigger):
        if not self.active:
            self.get_logger().warning(f"{self.name} not active: cannot provide data {msg.data}")
            return
        # call on get callback
        if msg.data in self.__callbacks:
            self.get_logger().debug(f"calling on-get callback for {msg.data}")
            self.__callbacks[msg.data](msg)
        #
        super()._data_trigger(msg)
