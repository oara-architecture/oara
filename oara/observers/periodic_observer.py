from abc import ABC, abstractmethod

import rclpy_lifecycle
from rclpy.parameter import Parameter

from .observer import Observer

class PeriodicObserver(Observer):
    """ A ROS2 OARA Periodic Observer.

    It is implemented as a ROS2 node that provides servers to access data.
    The default period can be changed using its ROS2 'period' parameter.
    """

    def __init__(self, name: str, period: float = 1.0):
        """
        :param name: observer name.
        :param period: default period for data update.
        """
        super().__init__(name)
        self.add_parameter('period', Parameter.Type.DOUBLE, 'Period of the observer update', default_value=period)
        self.update_model({'pattern': 'periodic-observer'})

    def on_configure(self, state: rclpy_lifecycle.State) -> rclpy_lifecycle.LifecycleNode.CallbackReturn:
        self.__period = self.get_parameter("period").value
        self.get_logger().debug(f"-- period: {self.__period}")
        return super().on_configure(state)

    def on_activate(self, state: rclpy_lifecycle.State) -> rclpy_lifecycle.LifecycleNode.CallbackReturn:
        self.create_timer(self.__period, self.update)
        return super().on_activate(state)

    ###------------------------ UPDATE ---###
    @abstractmethod
    def update(self):
        """ This method is periodically called to update data values. """
        pass