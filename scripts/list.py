#!/usr/bin/env python3
import rclpy

from oara.cli.list import List

def main():
    rclpy.init()
    node = List("oara_cli_list")
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        #node.console.print()
        pass

if __name__ == '__main__':
    main()