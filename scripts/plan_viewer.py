#!/usr/bin/env python3

import rclpy
import rclpy.node
from rcl_interfaces.msg import ParameterDescriptor, ParameterType

from oara_interfaces.msg import Plan, Task, TaskOrder, GoalState

import pygraphviz as pgv

from rclpy.parameter import Parameter

def state2str(state: int):
    if state == GoalState.GOAL_FORMULATED:
        return "FORMULATED"
    if state == GoalState.GOAL_SELECTED:
        return "SELECTED"
    if state == GoalState.GOAL_EXPANDED:
        return "EXPANDED"
    if state == GoalState.GOAL_COMMITTED:
        return "COMMITTED"
    if state == GoalState.GOAL_DISPATCHED:
        return "DISPATCHED"
    if state == GoalState.GOAL_EVALUATED:
        return "EVALUATED"
    if state == GoalState.GOAL_FINISHED:
        return "FINISHED"
    if state == GoalState.GOAL_DROPPED:
        return "DROPPED"
    return "UNKNOWN"

def state2color(state: int):
    if state == GoalState.GOAL_FORMULATED:
        return "brown"
    if state == GoalState.GOAL_SELECTED:
        return "darkorange"
    if state == GoalState.GOAL_EXPANDED:
        return "darkorange"
    if state == GoalState.GOAL_COMMITTED:
        return "darkorange"
    if state == GoalState.GOAL_DISPATCHED:
        return "darkslategray2"
    if state == GoalState.GOAL_EVALUATED:
        return "darkorange"
    if state == GoalState.GOAL_FINISHED:
        return "chartreuse4"
    if state == GoalState.GOAL_DROPPED:
        return "dimgrey"
    return "black"

class PlanViewer(rclpy.node.Node):
    def __init__(self, node_name: str):
        super().__init__(node_name)
        self.__sub = self.create_subscription(Plan, "plan", self.__plan_callback, 1)
        self.declare_parameter("output", value="plan.pdf", 
            descriptor=ParameterDescriptor(name="output", type=ParameterType.PARAMETER_STRING, description="output file name"))

    def __plan_callback(self, msg: Plan):
        G = pgv.AGraph(directed=True)
        for t in msg.tasks:
            t: Task
            G.add_node(t.index, label=f"[{t.index}:{t.actor}]\n{t.goal}\n{state2str(t.state)}", color=state2color(t.state))
        for e in msg.ordering:
            e: TaskOrder
            G.add_edge(e.before, e.after)
        G.layout(prog="dot")
        G.draw(self.get_parameter("output").value)

def main():
    rclpy.init()
    node = PlanViewer("plan_viewer")
    rclpy.spin(node)

if __name__ == '__main__':
    main()