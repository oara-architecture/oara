#!/usr/bin/env python3
import argparse
import rclpy
from rclpy.logging import LoggingSeverity
from oara.actors.pub_sub_controller import PubSubController
from oara.cli.msg_types import request_msg_from_name
from oara.internal.main import main as oara_main
from oara.cli.cli import CLI

def main(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('task',
        default="oara_std_interfaces/msg/StringGoal", type=str,
        help='Request message type defining the accepted task; e.g., "oara_std_interfaces/msg/StringGoal"')

    args, ros_args = parser.parse_known_args(args)

    cli = CLI()
    cli.header()

    rclpy.init(args=ros_args)

    try:
        msg_module = request_msg_from_name(args.task)
    except Exception:
        cli.console.print_exception(show_locals=True)

    oara_main(PubSubController, msg_module)


if __name__ == '__main__':
    main()