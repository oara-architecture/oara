#!/usr/bin/env python3
import argparse
import argcomplete

import rclpy
import rclpy.utilities
from ros2node.api import NodeNameCompleter
from ros2topic.api import TopicTypeCompleter, TopicMessagePrototypeCompleter

from oara_interfaces.msg import GoalState
from oara.cli.send_goal import SendGoal
from oara.cli.cli import CLI

'''
class GoalMessagePrototypeCompleter:
    def __init__(self, *, task_type_key=None):
        self.task_type_key = task_type_key

    def __call__(self, prefix, parsed_args, **kwargs):
        formulate_srv = get_service(getattr(parsed_args, self.task_type_key))
        default_goal = formulate_srv.Request().goal
        return [message_to_yaml(default_goal)]
'''

def main(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('actor', type=str, help='Actor name').completer = NodeNameCompleter()

    arg = parser.add_argument('task', help='Task type of the actor; e.g., "std_msgs/String"')
    arg.completer = TopicTypeCompleter()

    arg = parser.add_argument('goal', nargs='?', default='{}', help='Goal to send')
    arg.completer = TopicMessagePrototypeCompleter(topic_type_key='task')

    parser.add_argument('--sleep', type=float, default=1.0,
        help="Sleep a given amount of seconds between each request")
    
    argcomplete.autocomplete(parser)
    arguments = parser.parse_args(rclpy.utilities.remove_ros_args(args)[1:])

    cli = CLI()
    cli.header()

    rclpy.init()
    node = SendGoal('oara_send_goal')
    node.get_logger().set_level(rclpy.logging.LoggingSeverity.DEBUG)

    if not node.connect(arguments.actor):
        node.get_logger().error(f"Cannot find topic oara/request in node {arguments.actor}")
        return

    rclpy.spin_once(node)

    try:
        node.send(request=arguments.task, msg=arguments.goal)
        state = node.result()
        if state.state == GoalState.FINISHED:
            node.get_logger().info(f"FINISHED {state}")
            return

        elif state.state == GoalState.DROPPED:
            node.get_logger().warn(f"DROPPED {state}")
            return

        elif state.state == GoalState.FORMULATED:
            node.get_logger().warn(f"FORMULATED {state}")
            raise KeyboardInterrupt()

        else:
            node.get_logger().warn(f"UNKNOWN {state}")
            raise KeyboardInterrupt()

    except KeyboardInterrupt:
        node.drop()

    except Exception as ex:
        node.get_logger().error(f"{ex}")
        cli.console.print_exception(show_locals=True)

if __name__ == '__main__':
    main()