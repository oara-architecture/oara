#include <string>
#include <iostream>
#include <sstream>

#include <rclcpp/logging.hpp>
#include <rclcpp/qos.hpp>

#include <oara_interfaces/msg/goal_state.hpp>
#include <std_msgs/msg/string.hpp>

#include "oara/actor_client.hpp"
#include "oara/utils.hpp"
#include "oara/thread_safe_queue.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

namespace oara
{

AbstractActorClient::AbstractActorClient(rclcpp::Node::SharedPtr node, 
    const std::string& name, int timeout_sec)
    : node(node)
    , name(name)
    , qos(10)
    , alive(false)
    , timeout(timeout_sec)
{
    this->mutex_cb_group = this->node->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);

    std::stringstream sfbck; sfbck << name << "/oara/response";
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] creating subscription " << sfbck.str());
    feedback_options_.event_callbacks.liveliness_callback = std::bind(&AbstractActorClient::feedback_liveliness, this, _1);
    //feedback_options_.initialize();
    //std::chrono::milliseconds liveliness_lease_duration(1000);
    //RMW_QOS_POLICY_HISTORY_KEEP_LAST,
    qos.reliability(RMW_QOS_POLICY_RELIABILITY_RELIABLE);
    qos.durability(RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL);
    //qos.liveliness_lease_duration(liveliness_lease_duration);
    qos.liveliness(RMW_QOS_POLICY_LIVELINESS_AUTOMATIC);

    this->feedback_subscriber = this->node->create_subscription<oara_interfaces::msg::GoalState>(sfbck.str(),
        qos, std::bind(&AbstractActorClient::feedback_callback, this, _1),
        feedback_options_);
}

AbstractActorClient::~AbstractActorClient() {}

void AbstractActorClient::feedback_liveliness(rclcpp::QOSLivelinessChangedInfo& event) {
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] liveliness event; alive count: " << event.alive_count);
    if ( (event.alive_count_change > 0) && (event.alive_count > 0) ) {
        RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] connected to client " << this->name);
        this->alive = true;
    }
    else if ( (event.alive_count_change < 0) && (event.alive_count < 1) ) {
        RCLCPP_WARN_STREAM(node->get_logger(), "[client " << this->name << "] disconnected from client " << this->name);
        this->alive = false;
    }
}

bool AbstractActorClient::wait_connection() {
    rclcpp::Time start = this->node->get_clock()->now();
    rclcpp::Time now = this->node->get_clock()->now();
    rclcpp::Rate rate(10.0);
    while ( (! this->alive) && (now - start) < timeout ) {
        rate.sleep();
        now = this->node->get_clock()->now();
    }
    return this->alive;
}

bool AbstractActorClient::traverse_lifecycle(const std::string& task_id) {
    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] selecting task " << task_id);
    auto select_result = this->select(task_id);
    //task.state = GoalLifecycleState(result.state.state)
    if (! select_result) return false;
            
    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] expanding task " << task_id);
    auto expand_result = this->expand(task_id);
    //task.state = GoalLifecycleState(result.state.state)
    if (! expand_result) return false;

    auto state = this->get_state(task_id);
    auto plan_id = state.expansions[0].plan_id;
    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] commiting task " << task_id);
    auto commit_result = this->commit(task_id, plan_id);
    //task.state = GoalLifecycleState(result.state.state)
    if (! commit_result) return false;

    RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] dispatching task " << task_id);
    auto dispatch_result = this->dispatch(task_id);
    return dispatch_result;
}

void AbstractActorClient::feedback_callback(const oara_interfaces::msg::GoalState::SharedPtr msg) {
    RCLCPP_INFO_STREAM(this->node->get_logger(), "[client " << this->name << "] Received feedback " << msg->goal_id << " [" << oara::state2string(msg->state) << "]");
    if (this->goal_events.find(msg->goal_id) == this->goal_events.end())
        this->goal_events[msg->goal_id];// = oara::queue<oara_interfaces::msg::GoalState>();
    this->goal_events[msg->goal_id].push(*msg);
}

oara_interfaces::msg::GoalState AbstractActorClient::get_state(std::string id) {
    auto it = goal_states.find(id);
    if (it == goal_states.end()) {
        auto state = oara_interfaces::msg::GoalState();
        state.state = oara_interfaces::msg::GoalState::UNKNOWN;
        return state;
    }
    else
        return it->second;
}

oara_interfaces::msg::GoalState AbstractActorClient::result(const std::string& goal_id) {
    RCLCPP_INFO_STREAM(this->node->get_logger(), "[client " << this->name << "] Waiting for result of " << goal_id);
    auto state = this->get_state(goal_id);
    if (state.state != oara_interfaces::msg::GoalState::DISPATCHED) {
        RCLCPP_ERROR_STREAM(this->node->get_logger(), "[client " << this->name << "] goal " << goal_id << " not DISPATCHED");
        return state;
    }
    int s = state.state;
    while ((s != oara_interfaces::msg::GoalState::FINISHED) 
            && (s != oara_interfaces::msg::GoalState::DROPPED) 
            && (s != oara_interfaces::msg::GoalState::FORMULATED)) {
        oara_interfaces::msg::GoalState msg =  this->goal_events[goal_id].pop();
        RCLCPP_DEBUG_STREAM(this->node->get_logger(), "[client " << this->name << "] received goal " << msg.goal_id << " state " << state2string(msg.state));
        this->goal_states[msg.goal_id] = msg;
        s = msg.state;
    }
    return this->get_state(goal_id);
}

}  // namespace oara
