#include <chrono>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>
#include <oara_std_interfaces/msg/string_goal.hpp>

#include "oara/actor_client.hpp"
#include "oara/utils.hpp"

#include "oara/impl/actor_client.hpp"

using namespace std::chrono_literals;

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);
    rclcpp::Node::SharedPtr node = std::make_shared<rclcpp::Node>("test_string_client");
    auto r = rcutils_logging_set_logger_level(node->get_logger().get_name(), RCUTILS_LOG_SEVERITY_DEBUG);
    (void)r;

    rclcpp::executors::SingleThreadedExecutor executor;
     executor.add_node(node);

    std::thread t([&executor]() {
        executor.spin();
    });

    auto client = std::make_shared< oara::ActorClient<oara_std_interfaces::msg::StringGoal, std_msgs::msg::String> >(
            node, "print_controller", 3
        );

    client->wait_connection();

    auto task_id = oara::new_id();
    std_msgs::msg::String goal;
    goal.data = "Hello World from C++!";

    
    if (! client->traverse_lifecycle(task_id, goal))
        return -1;

    auto result = client->result(task_id);
    std::cout << "Received result " << result.goal_id << " " << oara::state2string(result.state) << std::endl;

    rclcpp::shutdown();
    t.join();
    return 0;
}