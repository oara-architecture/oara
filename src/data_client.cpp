#include <string>
#include <iostream>
#include <sstream>

#include <rclcpp/logging.hpp>
#include <rclcpp/qos.hpp>

#include "oara/data_client.hpp"
#include "oara/utils.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

namespace oara
{

AbstractDataClient::AbstractDataClient(rclcpp::Node::SharedPtr node, 
    const std::string& observer, int timeout_sec)
    : node(node)
    , observer(observer)
    , qos(10)
    , alive(false)
    , timeout(timeout_sec)
{
    this->cb_group = this->node->create_callback_group(rclcpp::CallbackGroupType::Reentrant);

    std::stringstream s; s << observer << "/data/trigger";
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] creating trigger " << s.str());

    //RMW_QOS_POLICY_HISTORY_KEEP_ALL,
    qos.keep_all();
    qos.reliability(RMW_QOS_POLICY_RELIABILITY_RELIABLE);
    qos.durability(RMW_QOS_POLICY_DURABILITY_VOLATILE);
    //qos.liveliness_lease_duration(liveliness_lease_duration);
    qos.liveliness(RMW_QOS_POLICY_LIVELINESS_AUTOMATIC);

    this->trigger_publisher = this->node->create_publisher<oara_interfaces::msg::DataTrigger>(s.str(), qos);

    this->subscriber_options_.event_callbacks.liveliness_callback = std::bind(&AbstractDataClient::feedback_liveliness, this, _1);
}

AbstractDataClient::~AbstractDataClient() {}

void AbstractDataClient::feedback_liveliness(rclcpp::QOSLivelinessChangedInfo& event) {
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] liveliness event; alive count: " << event.alive_count);
    if ( (event.alive_count_change > 0) && (event.alive_count > 0) ) {
        RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->observer << "] connected to client " << this->observer);
        this->alive = true;
    }
    else if ( (event.alive_count_change < 0) && (event.alive_count < 1) ) {
        RCLCPP_WARN_STREAM(node->get_logger(), "[client " << this->observer << "] disconnected from client " << this->observer);
        this->alive = false;
    }
}

bool AbstractDataClient::wait_connection() {
    rclcpp::Time start = this->node->get_clock()->now();
    rclcpp::Time now = this->node->get_clock()->now();
    rclcpp::Rate rate(10.0);
    while ( (! this->alive) && (now - start) < timeout ) {
        rate.sleep();
        now = this->node->get_clock()->now();
    }
    return this->alive;
}

void AbstractDataClient::header_callback(std::string data, const std_msgs::msg::Header::SharedPtr msg) {
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->observer << "] received data " << data << " header");
    this->headers[data] = *msg;
    std::unique_lock<std::mutex> lk(mutex[data]);
    this->header_received[data] = true;
    lk.unlock();
    this->header_conditions[data].notify_one();
}


}  // namespace oara
