#include <chrono>

#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/int32.hpp>

#include "oara/data_client.hpp"
//#include "oara/impl/data_client.hpp"

using namespace std::chrono_literals;

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);
    rclcpp::Node::SharedPtr node = std::make_shared<rclcpp::Node>("test_data_client");
    auto r = rcutils_logging_set_logger_level(node->get_logger().get_name(), RCUTILS_LOG_SEVERITY_DEBUG);
    (void)r;

    rclcpp::executors::SingleThreadedExecutor executor;
     executor.add_node(node);

    std::thread t([&executor]() {
        executor.spin();
    });

    auto client = std::make_shared< oara::DataClient<std_msgs::msg::Int32> >(
            node, "travel_observer", 3
        );
    client->add_data("loc", "loc");

    client->wait_connection();

    auto d = client->get_data("loc");
    RCLCPP_INFO_STREAM(node->get_logger(), "header: " << d.first.frame_id << " / " << d.first.stamp.sec);
    RCLCPP_INFO_STREAM(node->get_logger(), "value: " << d.second.data);

    rclcpp::shutdown();
    t.join();
    return 0;
}