#include <string>
#include <iostream>
#include <sstream>

#include <rclcpp/logging.hpp>
#include <rclcpp/qos.hpp>

#include <oara_interfaces/msg/goal_state.hpp>
#include <std_msgs/msg/string.hpp>

#include "oara/goal_client.hpp"
#include "oara/utils.hpp"
#include "oara/thread_safe_queue.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

namespace oara
{

GoalClient::GoalClient(rclcpp::Node::SharedPtr node, 
    const std::string& name, int timeout_sec)
    : node(node)
    , name(name)
    , qos(10)
    , alive(false)
    , timeout(timeout_sec)
{
    this->mutex_cb_group = this->node->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);

    std::stringstream sfbck; sfbck << name << "/oara/response";
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] creating subscription " << sfbck.str());
    feedback_options_.event_callbacks.liveliness_callback = std::bind(&GoalClient::feedback_liveliness, this, _1);
    //feedback_options_.initialize();
    //std::chrono::milliseconds liveliness_lease_duration(1000);
    //RMW_QOS_POLICY_HISTORY_KEEP_LAST,
    qos.reliability(RMW_QOS_POLICY_RELIABILITY_RELIABLE);
    qos.durability(RMW_QOS_POLICY_DURABILITY_TRANSIENT_LOCAL);
    //qos.liveliness_lease_duration(liveliness_lease_duration);
    qos.liveliness(RMW_QOS_POLICY_LIVELINESS_AUTOMATIC);

    this->feedback_subscriber = this->node->create_subscription<oara_interfaces::msg::GoalState>(sfbck.str(),
        qos, std::bind(&GoalClient::feedback_callback, this, _1),
        feedback_options_);
}

GoalClient::~GoalClient() {}

void GoalClient::feedback_liveliness(rclcpp::QOSLivelinessChangedInfo& event) {
    RCLCPP_DEBUG_STREAM(node->get_logger(), "[client " << this->name << "] liveliness event; alive count: " << event.alive_count);
    if ( (event.alive_count_change > 0) && (event.alive_count > 0) ) {
        RCLCPP_INFO_STREAM(node->get_logger(), "[client " << this->name << "] connected to client " << this->name);
        this->alive = true;
    }
    else if ( (event.alive_count_change < 0) && (event.alive_count < 1) ) {
        RCLCPP_WARN_STREAM(node->get_logger(), "[client " << this->name << "] disconnected from client " << this->name);
        this->alive = false;
    }
}

bool GoalClient::wait_connection() {
    rclcpp::Time start = this->node->get_clock()->now();
    rclcpp::Time now = this->node->get_clock()->now();
    rclcpp::Rate rate(10.0);
    while ( (! this->alive) && (now - start) < timeout ) {
        rate.sleep();
        now = this->node->get_clock()->now();
    }
    return this->alive;
}

void GoalClient::feedback_callback(const oara_interfaces::msg::GoalState::SharedPtr msg) {
    RCLCPP_INFO_STREAM(this->node->get_logger(), "[client " << this->name << "] Received feedback " << msg->goal_id << " [" << oara::state2string(msg->state) << "]");
    if (this->goal_events.find(msg->goal_id) == this->goal_events.end())
        this->goal_events[msg->goal_id];
    this->goal_events[msg->goal_id].push(*msg);
    this->goal_states[msg->goal_id] = *msg;
    rclcpp::Time t(msg->header.stamp);
    timelines[msg->goal_id][t.seconds()] = msg->state;
}

oara_interfaces::msg::GoalState GoalClient::get_state(std::string id) const {
    auto it = goal_states.find(id);
    if (it == goal_states.end()) {
        auto state = oara_interfaces::msg::GoalState();
        state.state = oara_interfaces::msg::GoalState::UNKNOWN;
        return state;
    }
    else
        return it->second;
}

std::vector<std::string> GoalClient::get_goal_ids() const {
    std::vector<std::string> ids;
    for (auto& [i, _] : goal_states)
        ids.push_back(i);
    return ids;
}


void GoalClient::clear_timelines() {
    timelines.clear();
}

}  // namespace oara
