#include <string>
#include <iostream>
#include <sstream>

#include <rclcpp/logging.hpp>
#include <rclcpp/qos.hpp>

#include <lifecycle_msgs/msg/state.hpp>
#include "oara/module_info.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;

namespace oara
{

ModuleInfoClient::ModuleInfoClient(rclcpp::Node::SharedPtr node)
    : node(node)
    , qos(100)
{
    this->info_cb_group = this->node->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
    qos.reliability(RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT);
    qos.durability(RMW_QOS_POLICY_DURABILITY_VOLATILE);
    qos.liveliness(RMW_QOS_POLICY_LIVELINESS_AUTOMATIC);
    this->module_info_subscription = this->node->create_subscription<oara_interfaces::msg::ModuleInfo>(
        "oara/module_info",
        qos, 
        std::bind(&ModuleInfoClient::module_info_callback, this, _1));
}

ModuleInfoClient::~ModuleInfoClient() {}

void ModuleInfoClient::module_info_callback(const oara_interfaces::msg::ModuleInfo::SharedPtr msg) {
    auto name = msg->header.frame_id;
    auto new_module = (this->module_info.find(name) == this->module_info.end());

    this->module_info[name] = *msg;
    if (msg->type == oara_interfaces::msg::ModuleInfo::ACTOR)
        this->actor_info[msg->header.frame_id] = *msg;
    else if (msg->type == oara_interfaces::msg::ModuleInfo::OBSERVER)
        this->observer_info[msg->header.frame_id] = *msg;

    if (new_module) {
        this->change_state_clients[name] = this->node->create_client<lifecycle_msgs::srv::ChangeState>(name+"/change_state");
        if (this->is_actor(name))
            this->goal_clients[name] = std::make_shared<GoalClient>(this->node, name, 1);
    }
}

std::string ModuleInfoClient::get_state_str(const std::string& name) const {
    return this->module_info.at(name).state.label;
}

bool ModuleInfoClient::is_configured(const std::string& name) const {
    auto s = this->module_info.at(name).state.id;
    return (s == lifecycle_msgs::msg::State::PRIMARY_STATE_INACTIVE)
        || (s == lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE);
}

bool ModuleInfoClient::is_finalized(const std::string& name) const {
    auto s = this->module_info.at(name).state.id;
    return (s == lifecycle_msgs::msg::State::PRIMARY_STATE_FINALIZED);
}

bool ModuleInfoClient::is_active(const std::string& name) const {
    auto s = this->module_info.at(name).state.id;
    return (s == lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE);
}

std::vector<std::string> ModuleInfoClient::get_module_names() const {
    std::vector<std::string> modules;
    for (const auto& [key, _] : module_info) {
        modules.push_back(key);
    }
    return modules;
}

oara_interfaces::msg::ModuleInfo ModuleInfoClient::get_info(const std::string& name) const {
    return this->module_info.at(name);
}

bool ModuleInfoClient::is_actor(const std::string& name) const {
    return this->module_info.at(name).type == oara_interfaces::msg::ModuleInfo::ACTOR;
}

bool ModuleInfoClient::change_module_state(const std::string& name, const std::string& transition) {
    auto request = std::make_shared<lifecycle_msgs::srv::ChangeState::Request>();
    request->transition.label = transition;

    auto& client = this->change_state_clients[name];
    if (!client->wait_for_service(1s)) {
        RCLCPP_ERROR(this->node->get_logger(), "change_state service not available for %s", name.c_str());
        return false;
    }
    auto result = client->async_send_request(request);
    // if (this->executor->spin_until_future_complete(result, 1s) == rclcpp::FutureReturnCode::SUCCESS) {
    //     return result.get()->success;
    // } else {
    //     RCLCPP_ERROR(this->ros_node->get_logger(), "failed to call service change_state service for %s", node_name.c_str());
    //     return false;
    // }
    return true;
}

std::vector<oara_interfaces::msg::GoalState> ModuleInfoClient::get_goals(const std::string& name) const {
    std::vector<oara_interfaces::msg::GoalState> states;
    try {
        for (auto i: this->goal_clients.at(name)->get_goal_ids()) {
            states.push_back(this->goal_clients.at(name)->get_state(i));
        }
    }
    catch (std::out_of_range) {}
    return states;
}


void ModuleInfoClient::clear_goal_timeline(const std::string& name) {
    try {
        goal_clients.at(name)->clear_timelines();
    } catch (...) {}
}

GoalClient::Timeline ModuleInfoClient::get_goal_timeline(const std::string& name, const std::string& goal_id) const {
    try {
        return goal_clients.at(name)->get_timeline(goal_id);
    } catch (...) {
        return GoalClient::Timeline();
    }
}

} // namespace