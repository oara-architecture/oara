#include <iostream>

#include "oara/utils.hpp"

int main(int argc, char* argv[]) {
    (void)argc; (void) argv;
    
    auto task_id = oara::new_id();
    std::cout << "uuid: " << task_id << std::endl;
}