#include <sstream>
#include <iostream>
#include <random>
#include <iomanip> 

#include "oara/utils.hpp"

namespace oara {

std::string new_id() {
    std::random_device rd;
    auto seed_data = std::array<int, std::mt19937::state_size> {};
    std::generate(std::begin(seed_data), std::end(seed_data), std::ref(rd));
    std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
    std::mt19937 generator(seq);

    std::array<uint8_t, 16> data{ { 0 } };
    std::uniform_int_distribution<uint32_t>  distribution;
    uint8_t bytes[16];
    for (int i = 0; i < 16; i += 4)
        *reinterpret_cast<uint32_t*>(bytes + i) = distribution(generator);
    // variant must be 10xxxxxx
    bytes[8] &= 0xBF;
    bytes[8] |= 0x80;
    // version must be 0100xxxx
    bytes[6] &= 0x4F;
    bytes[6] |= 0x40;
    std::copy(std::begin(bytes), std::end(bytes), std::begin(data));
    std::stringstream s;
    // manipulate stream as needed
    s << std::hex << std::setfill(static_cast<char>('0'))
        << std::setw(2) << (int)data[0]
        << std::setw(2) << (int)data[1]
        << std::setw(2) << (int)data[2]
        << std::setw(2) << (int)data[3]
        << '-'
        << std::setw(2) << (int)data[4]
        << std::setw(2) << (int)data[5]
        << '-'
        << std::setw(2) << (int)data[6]
        << std::setw(2) << (int)data[7]
        << '-'
        << std::setw(2) << (int)data[8]
        << std::setw(2) << (int)data[9]
        << '-'
        << std::setw(2) << (int)data[10]
        << std::setw(2) << (int)data[11]
        << std::setw(2) << (int)data[12]
        << std::setw(2) << (int)data[13]
        << std::setw(2) << (int)data[14]
        << std::setw(2) << (int)data[15];
    return s.str();
}

std::string state2string(const oara_interfaces::msg::GoalState& msg) {
    return state2string(msg.state);
}

std::string state2string(int state) {
    switch (state)
    {
    case oara_interfaces::msg::GoalState::UNKNOWN:
        return "UNKNOWN";
    case oara_interfaces::msg::GoalState::FORMULATED:
        return "FORMULATED";
    case oara_interfaces::msg::GoalState::SELECTED:
        return "SELECTED";
    case oara_interfaces::msg::GoalState::EXPANDED:
        return "EXPANDED";
    case oara_interfaces::msg::GoalState::COMMITTED:
        return "COMMITTED";
    case oara_interfaces::msg::GoalState::DISPATCHED:
        return "DISPATCHED";
    case oara_interfaces::msg::GoalState::EVALUATED:
        return "EVALUATED";
    case oara_interfaces::msg::GoalState::FINISHED:
        return "FINISHED";
    case oara_interfaces::msg::GoalState::DROPPED:
        return "DROPPED";
    default:
        return "UNKNOWN";
    }
}

}
